<!DOCTYPE html>
<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Lapexperience</title>
        <meta name="generator" content="Bootply">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="./css/bootstrap.css" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<link href="./css/holo.css" rel="stylesheet">

    </head>
    
    <!-- HTML code from Bootply.com editor -->
    
    <body>
        
        <div class="page-container">
  

      
    <div class="col-sm-12">
      <div class="row row-offcanvas row-offcanvas-left">
        

  	
        <!-- main area -->
        <div class="col-xs-12 col-sm-9" data-spy="scroll" data-target="#sidebar-nav">
          
          <div class="row">
           <div class="col-sm-6">
             
                <div class="panel panel-default">
                   <div class="panel-heading"><img src="assets/img/100.gif" class="img-circle">
                        <div class="clearfix"></div>
                        Welcome Aleksei Zimin<br />
                        Journalist
                    </div>
                    <div class="clearfix"></div>

                    <div class="panel-subheading">
                        <img src="assets/img/50.gif" class="img-circle pull-left">
                        <span class="event-date">31-01-2015 hasta 03-02-2015</span><br/>
                        <span class="event-name">Flying Spur V8 Driving Experience</span>
                    </div>
                        <div class="clearfix"></div>



                    <div class="panel-body">
                      <iframe width="100%" height="auto" src="https://www.youtube.com/embed/UkfoPJI1nL8" frameborder="0" allowfullscreen></iframe>
 
                      
                    </div>
                 </div>
        
            </div>
            
          </div><!--/row-->
          
          

          <hr>
    
  
          
    </div><!--/.row-->
      <!-- top navbar -->
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
      <div class="navbar-header">
           <button type="button" class="navbar-toggle op1" data-toggle="offcanvas" data-target=".sidebar-nav"></button>
           <button type="button" class="navbar-toggle op2" data-toggle="offcanvas" data-target=".sidebar-nav"></button>
           <button type="button" class="navbar-toggle op3" data-toggle="offcanvas" data-target=".sidebar-nav"></button>
           <button type="button" class="navbar-toggle op4" data-toggle="offcanvas" data-target=".sidebar-nav"></button>
           <button type="button" class="navbar-toggle op5" data-toggle="offcanvas" data-target=".sidebar-nav"></button>
      </div>
    </div>
  </div><!--/.container-->
</div><!--/.page-container-->



</div>


            <div id="push"></div>
        
        <script type="text/javascript" async="" src="assets/js/quant.js"></script>
		</script><script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.js"></script>
        
        <script type="text/javascript">
        
        $(document).ready(function() {
            $('[data-toggle=offcanvas]').click(function() {
				$('.row-offcanvas').toggleClass('active');
			});
			  
			$('.btn-toggle').click(function() {
				$(this).find('.btn').toggleClass('active').toggleClass('btn-default').toggleClass('btn-primary');
			});
        });
        
        </script>
        
    
</body></html>