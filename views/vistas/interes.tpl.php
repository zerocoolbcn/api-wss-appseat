<script>
	
	$('.layer').click(function(){

		var nombre = $(this).attr('data-name');	
		var img = $(this).attr('data-imagen');
		var gps = $(this).attr('data-gps');	
		var texto = $(this).attr('data-texto');
		
		window.gpsCoredenada = gps;

		$('.layerInfo').css('display','');
		$('.imga').attr('src',img);
		$('.layerInfoNAME').html(nombre);
		$('.layerInfoTEXTO').html(texto);

	})


	$('.btngps').click(function(){

		app.nave(window.gpsCoredenada);

	})


	$('.closeInteres').click(function(){

		$('.layerInfo').css('display','none')
	})

</script> <?php
	

  if($_GET['lang']=="pr"){
    $idioma = "pt";
  }elseif($_GET['lang']=="gr"){
    $idioma = "de";
  }else
  {
    $idioma = $_GET['lang'];
  }


	$lang['es'][1] = "Iniciar navegación";
	$lang['en'][1] = "Start navigation";
	$lang['it'][1] = "Inizio navigazione";
	$lang['fr'][1] = "Début navigation";
	$lang['de'][1] = "Navigation starten";
	$lang['pt'][1] = "Iniciar navegação";


	$lang['es'][2] = "Ver";
	$lang['en'][2] = "View";
	$lang['it'][2] = "Vedere";
	$lang['fr'][2] = "Voir";
	$lang['de'][2] = "Sehen";
	$lang['pt'][2] = "Ver";


	$lang['es'][3] = "Clima";
	$lang['en'][3] = "Weather";
	$lang['it'][3] = "Tempo";
	$lang['fr'][3] = "Temps";
	$lang['de'][3] = "Wetter";
	$lang['pt'][3] = "Tempo";



?>
<div class="layerInfo" style="display:none">
	

	<i class="fa fa-times closeInteres"></i>
	
	<center><div  class="layerInfoNAME"></div>
		<img src="" class="imga" >
		<div  class="layerInfoTEXTO"></div>

		<div class="btngps" ><?php echo $lang[$idioma][1] ?></div>
	</center>

</div>
<div class="togglebox">

<?php

	function wordlimit($string, $length = 50, $ellipsis = "")
	{
	    $words = explode(' ', $string);
	    if (count($words) > $length)
	    {
	            return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
	    }
	    else
	    {
	            return $string;
	    }
	}

	 function objectToArray($d) {
		 if (is_object($d)) {
		 // Gets the properties of the given object
		 // with get_object_vars function
		 $d = get_object_vars($d);
		 }
		 
		 if (is_array($d)) {
		 /*
		 * Return array converted to object
		 * Using __FUNCTION__ (Magic constant)
		 * for recursive call
		 */
		 return array_map(__FUNCTION__, $d);
		 }
		 else {
		 // Return array
		 return $d;
		 }
	 }

function normaliza ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}


	$query = Database::query("SELECT * FROM categorias");
		
	if(count($query)>0){
			
	for ($i=0;$i<count($query);$i++){	
	
	$json = json_decode(utf8_encode($query[$i]['contenido']));

	$json = objectToArray($json);

	$queryInteres = Database::query("SELECT * FROM intereseventos WHERE cat='".$query[$i]['id']."'");
?>

  <input id="toggle<?php echo $i+1 ?>" type="radio" name="toggle" />
  <label for="toggle<?php echo $i+1 ?>"> <?php echo $json[$idioma] ?> <span class="redondo"> <span class="num"><?php  echo count($queryInteres) ?></span></span></label>
  
  <section id="content<?php echo $i+1 ?>">
	

	<?php 

		for ($B=0;$B<count($queryInteres);$B++)
		{	

			
			$json_titulo = json_decode(utf8_encode($queryInteres[$B]['titulo']));
			$json_texto   = json_decode(utf8_encode($queryInteres[$B]['contenido']));
			
		
			$json_titulo = objectToArray($json_titulo);
			$json_texto = objectToArray($json_texto);

			$titulo = $json_titulo[$idioma];
			$texto  = $json_texto[$idioma];

	?> 


	<div class="item">

		<h3 style="margin:0px"><?php e(wordlimit($titulo)); ?></h3>
			<small style="margin:0px"><?php e($texto) ?></small><br>
			<small style="margin:0px; color:#E1E1E1"><?php echo $queryInteres[$B]['Direccion']?>  <?php echo $queryInteres[$B]['poblacion']?></small><br>
				<div class="gps"  onclick="app.nave('<? e($queryInteres[$i]['Direccion']) ?> <?php echo $queryInteres[$B]['poblacion']?>')">
					<i class="fa  fa-location-arrow"></i> 
					GPS
				</div>
		<div class="ver layer" data-name="<?php e($titulo); ?>" data-gps="<?php e($queryInteres[$B]['Direccion']) ?> <?php echo $queryInteres[$B]['poblacion']?>" data-texto="<?php echo $texto ?>" data-imagen="http://backoffice.seatexperience.net/upload/interes/<?php echo $queryInteres[$B]['imagen']?>">
		<i class="fa fa-eye "></i> <?php echo $lang[$idioma][2] ?>
		</div>
		<?php if(!empty($queryInteres[$B]['poblacion'])){ ?><div class="clima tiempoLoad"  rel="<? e($queryInteres[$B]['Direccion']) ?>">
		<i class="fa fa-sun-o fa-1x"></i> <?php echo $lang[$idioma][3] ?>
		</div><?php } ?>
		
	</div>


	<?php 
	}

	?>
	
  </section>
  
  <?php	
						
		
	}	
			
	}else
	{
		echo "<center ><h3 style='color:#fff'>No hay categorias de interes </h3></center>";

	}

	$domain ="http://seatexperience.net";


?>

</div>



<style type="text/css" media="screen">
.item
{
	margin-left:10px;

}



span.num {
  position: relative;
  padding: 6px;
  top: -15px;
}
.togglebox {
  width: 100%;
  margin-top:74px;

  background: #fff;
  color:#666;
  transform: translateZ(0);
  box-shadow: 0 1px 1px rgba(0,0,0,.1);
}
span.redondo {
  background-color: rgb(201, 40, 46);
  color: white;
  border-radius: 19px;
  right: 27px;
  position: relative;
  float: right;
  width: 20px;
  /* height: 26px; */
  height: 20px;
  margin-top: 14px;
  padding: 6px;
}
input[type="radio"] {
  position: absolute;
  opacity: 0;
}

label {
  position: relative;
  display: block;
  height: 50px;
  line-height: 50px;
  padding: 0 20px;
  color:#666;
  font-size: 17px;
  font-weight: 700;
  border-top: 1px solid #ddd;
  background: #fff;
  cursor: pointer;
}

label[for*='1'] {
  border: 0;
}

label:after {
  content: '\f078';
  position: absolute;
  top: 0px;
  right: 20px;
  font-family: fontawesome;
  transform: rotate(90deg);
  transition: .3s transform;
}
.ver {
  float: left;
  padding: 11px;
  width:22%;
  background-color: rgb(221, 221, 221);
  margin-bottom: 23px;
  border-bottom: 3px solid rgb(201, 40, 46);
}
.gps {
  background-color: darkgrey;
  width: 30%;
  float: left;
  padding: 11px;
  color: #fff;
  margin-bottom: 23px;
  border-bottom: 3px solid rgb(201, 40, 46);
}
.clima {
  padding: 11px;
  background-color: aliceblue;
  width: 27%;
  float: left;
  margin-bottom: 23px;
  border-bottom: 3px solid rgb(201, 40, 46);
}
section {
  height: 0;
  transition: .3s all;
  overflow: hidden;
}

#toggle1:checked ~ label[for*='1']:after,
#toggle2:checked ~ label[for*='2']:after,
#toggle3:checked ~ label[for*='3']:after,
#toggle4:checked ~ label[for*='4']:after,
#toggle5:checked ~ label[for*='5']:after,
#toggle6:checked ~ label[for*='6']:after,
#toggle7:checked ~ label[for*='7']:after,
#toggle8:checked ~ label[for*='8']:after,
#toggle9:checked ~ label[for*='9']:after,
#toggle10:checked ~ label[for*='10']:after,
#toggle11:checked ~ label[for*='11']:after,
#toggle12:checked ~ label[for*='12']:after,
#toggle13:checked ~ label[for*='13']:after,
#toggle14:checked ~ label[for*='14']:after,
#toggle15:checked ~ label[for*='15']:after,
#toggle16:checked ~ label[for*='16']:after,
#toggle17:checked ~ label[for*='17']:after,
#toggle18:checked ~ label[for*='18']:after,
#toggle19:checked ~ label[for*='19']:after,
#toggle20:checked ~ label[for*='20']:after,
#toggle21:checked ~ label[for*='21']:after {
  transform: rotate(0deg);
}

#toggle1:checked ~ #content1,
#toggle2:checked ~ #content2,
#toggle3:checked ~ #content3,
#toggle4:checked ~ #content4,
#toggle5:checked ~ #content5,
#toggle6:checked ~ #content6,
#toggle7:checked ~ #content7,
#toggle8:checked ~ #content8,
#toggle9:checked ~ #content9,
#toggle10:checked ~ #content10,
#toggle11:checked ~ #content11,
#toggle12:checked ~ #content12,
#toggle13:checked ~ #content13,
#toggle14:checked ~ #content14,
#toggle15:checked ~ #content15
 {
  height: 100%;

}

p {
  margin: 15px 0;
  padding: 0 20px;
  font-size: 12px;
  line-height: 1.5;
}

</style>
