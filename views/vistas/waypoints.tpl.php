<script>
	
	$('.layer').click(function(){

		var nombre = $(this).attr('data-name');	
		var img = $(this).attr('data-imagen');
		var gps = $(this).attr('data-gps');	
		var texto = $(this).attr('data-texto');
		
		window.gpsCoredenada = gps;

		$('.layerInfo').css('display','');
		$('.imga').attr('src',img);
		$('.layerInfoNAME').html(nombre);
		$('.layerInfoTEXTO').html(texto);

	})


	$('.btngps').click(function(){

		app.nave(window.gpsCoredenada);

	})


	$('.closeInteres').click(function(){

		$('.layerInfo').css('display','none')
	})

</script>
<?php

	$lang['es'][1] = "Iniciar navegación";
	$lang['en'][1] = "Start navigation";
	$lang['it'][1] = "Inizio navigazione";
	$lang['fr'][1] = "Début navigation";
	$lang['al'][1] = "Navigation starten";
	$lang['po'][1] = "Iniciar navegação";


	$lang['es'][2] = "Ver";
	$lang['en'][2] = "See";
	$lang['it'][2] = "Vedere";
	$lang['fr'][2] = "Voir";
	$lang['al'][2] = "Sehen";
	$lang['po'][2] = "Ver";


	$lang['es'][3] = "Clima";
	$lang['en'][3] = "Weather";
	$lang['it'][3] = "Tempo";
	$lang['fr'][3] = "Temps";
	$lang['al'][3] = "Wetter";
	$lang['po'][3] = "Tempo";



?>
<div class="layerInfo" style="display:none">
	

	<i class="fa fa-times closeInteres fa-2x"></i>
	
	<center><div  class="layerInfoNAME"></div>
		<img src="" class="imga" >
		<div  class="layerInfoTEXTO"></div>

		<div class="btngps" ><?php echo $lang[$_GET['lang']][1] ?></div>
	</center>

</div>
<?php
function wordlimit($string, $length = 50, $ellipsis = "")
{
    $words = explode(' ', $string);
    if (count($words) > $length)
    {
            return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
    }
    else
    {
            return $string;
    }
}
?>

<BR><BR><BR>


		<br clear="all" > 

<div id="resulta" style="overflow:auto; height:300px">



<?php
	$query = Database::query("SELECT * FROM waypointevento WHERE idevento='".$_GET['idevento']."'");
	


		if(count($query)>0){
		
		
		for ($i=0;$i<count($query);$i++){
		
		
		?>

      <div class="desplegar" rel="<? e($i)?>"> 
          <div class="textMenu txtwaypoint">
           <i class="fa fa-map-marker"></i> <? e(ucwords (strtolower(wordlimit($query[$i][$_GET['lang']],4)))) ?>...
           </div > 
           
            <i class="fa fa-chevron-right m">
            </i>
          
        </div>
        
         <div id="div_<? e($i)?>" class="divocultoGPS" style="display:none; padding-bottom:10px">
        
       	<center> 
     	 




     	  <div class="iniciarGPS btnGPS initgps" onclick="app.nave('<? e($query[$i]['Direccion']) ?>')">
     	  	<div class="spaceTExt"><i class="fa  fa-location-arrow"></i> Iniciar GPS </div>
     	  </div>


     	  <div id="verfotoway" class="ver layer compartir btnGPSver" data-imagen="" data-texto="<? e(strtolower($query[$i][$_GET['lang']])) ?>" data-gps="<? e($query[$i]['Direccion']) ?>">
     	  	<div class="spaceTExt"><i class="fa fa-eye fa-2x"></i></div>
     	  </div>


     	  <div class="compartir btnGPS share">
     	  	<div class="spaceTExt"><i class="fa fa-share-alt-square fa-2x"></i></div>
     	  </div>


     	   <div class="spaceTExt btnGPS tiempoLoad" rel='<?php if(!empty($query[$i]['poblacion'])){e($query[$i]['poblacion']);}else{e("Barcelona");}  ?>'>
     	  		 <i class="fa fa-sun-o fa-2x"></i>
     	   </div>

        </center>
        

        <center><div class="menuBar"></div> 
 
        </div>
        
     					  <?	
						
		
	}	
			
			}else
			{
				echo "<center ><h3 style='color:#fff'>No hay waypoints </h3></center>";
			
			}

	$domain ="http://lapexperience.com/"
?>
</div>

<script type="text/javascript">
  var directionsDisplay;
  
  var directionsService = new google.maps.DirectionsService();
  var map;
  var oldDirections = [];
  var currentDirections = null;
  var coordenadasInicio = {
			Lat: 0,
			Lng: 0
  };
  var markers=[];
  var contentString=[];
  var arrayPuntos = [];
  var mymarker;
  var routeActual;
  
  function initialize() {
  
    var myOptions = {
      zoom: 8,
      center: new google.maps.LatLng(-33.879,151.235),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  styles : [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
	   
    }
	
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	directionsDisplay = new google.maps.DirectionsRenderer({ 
			map: map ,
			polylineOptions:{strokeColor:'<? e($color) ?>'},
			markerOptions:{visible:false}
	}); 
	 

	$.post("http://lapexperience.com/frontend/ajax/loadwaypoints.php",{id:'<?php e($_GET['idevento'])?>',idioma:'<? e('es')?>', path_siteRoot: '<?php e($path) ?>'},function(data){
		
		var data = $.parseJSON(data);

		
		var length = data.length;
			
		for (var i = 0; i < length; i++) {

			  
			  if(i==0){
				  
				map.setCenter(new google.maps.LatLng (data[i].lat,data[i].lng));	  
			  }

				var marker = new google.maps.Marker({
					position : new google.maps.LatLng (data[i].lat,data[i].lng),
					map : map,
					icon:
			   		{
						url: '<? echo $domain ?>'+data[i].icono
						
						
					},
					infoWindowIndex : i //<---Thats the extra attribute
				});


				var arrayP = [];
				arrayP['direccion'] = data[i].direccion;
				arrayP['imagen'] = data[i].imagen;
				arrayP['titulo'] = data[i].titulo;
				arrayP['lat'] = data[i].lat;
				arrayP['lon'] = data[i].lng;


		
				google.maps.event.addListener(marker, 'click', 
					function(event)
					{
					
						abrirblacklayer(arrayPuntos[this.infoWindowIndex])
					
					}
				);
		
				arrayPuntos.push(arrayP);
				markers.push(marker);			
			
		 }
	});
	
	
	
	
	
	
	
  }

  function abrirblacklayer(content)
  {
  		$('.layerInfoTEXTO').html("");
  		$('.layerInfoNAME').html("");
  		$('.imga').attr('src','');

  		console.log(content.titulo);
  	
		$('.layerInfo').css('display','');
		$('.imga').attr('src','<?php e($domain) ?>/backoffice/upload/info_eventos/waypoints/fotos/'+content.imagen);
		$('.layerInfoNAME').html(content.titulo);
	
		

  	
  	$('.blacklayer').css('display','');
  	$('.contentblacklayer').css('display','');

  	
  

  }

  $( document ).ready(function() {
	  initialize();
	  var posicionReal = $("#footer").offset();
	 var heightMap = window.innerHeight;

	  
	//  $("#map_canvas").css("height", heightMap);
	  $("#map_canvas").css("width", window.innerWidth);
	  $("#resulta").css("height", heightMap-380);
	  
	  
	 	
		
		function localizacion (posicion) {
			coordenadasInicio = {
				Lat: posicion.coords.latitude,
				Lng: posicion.coords.longitude
			}
			
			map.setCenter(new google.maps.LatLng(coordenadasInicio.Lat, coordenadasInicio.Lng));
			mymarker = new google.maps.Marker({
			   position: new google.maps.LatLng (coordenadasInicio.Lat,coordenadasInicio.Lng),
			   map: map,
			   icon:
			   		{
						url: "<? echo $domain ?>/frontend/images/icon_miposition.png",
						anchor: new google.maps.Point(23,23)
						
					}
				
			});
			
		}
		function errores(error) {
		
		}
	    if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(localizacion,errores);
		} else {
			
		}
		
		
		
  });
</script>

<div id="map_canvas" style="float:left;width:100%; height:290px; position:absolute">map</div>


<style type="text/css" media="screen">

p {
  margin: 15px 0;
  padding: 0 20px;
  font-size: 12px;
  line-height: 1.5;
}
i.fa.fa-sun-o.fa-2x {
  padding-top: 8px;
}

.wsContenido {

  background-color: rgb(201, 40, 46);
}
</style>

