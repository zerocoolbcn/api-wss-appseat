<div class="wrapper shadow">
  <div class="top">
    <div class="btn noselect" id="btn">F</div>
    <img id="big" src="" alt="">
    <p id="deg" class="text deg"></p>
    <input class="text" type="text" id="city" value="">
  </div>
  <div class="bot">
    <ul>
      <li>
        <h1 id="forecast0"></h1>
        <img id="forecastimg0" src=""></img>
        <p id="forecastdeg0"></p>
      </li>
      <li>
        <h1 id="forecast1"></h1>
        <img id="forecastimg1" src=""></img>
        <p id="forecastdeg1"></p>
      </li>
      <li>
        <h1 id="forecast2"></h1>
        <img id="forecastimg2" src=""></img>
        <p id="forecastdeg2"></p>
      </li>
      <li>
        <h1 id="forecast3"></h1>
        <img  id="forecastimg3" src=""></img>
        <p id="forecastdeg3"></p>
      </li>
    </ul>
  </div>
</div>
