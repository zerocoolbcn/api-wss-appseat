<br><br><br><center><div class="loader-box">
	<div class="lable">GPS DESACTIVADO</div>
	<div class="loader">
        <div class="element-animation">
        <img src="https://dl.dropboxusercontent.com/u/35259193/loader.png" width="1180" height="70";>
        </div>

	</div>
</div>
Es necesario el GPS activo!</center>

<div style="padding:20px" >
<h4>¿Como funciona?</h4>
Para utilizar esta funcionalidad es necesario que tengas activado la geolocalizacion por GPS en tu telefono movil, de lo contrario no podremos obtener datos para la telemetria.</div>
<style>
	
	.loader-box{
	width:200px;
	height:200px;
	margin:auto;
	margin-top:10px;
	text-align:center;
}

.lable{
	height:30px;
	line-height:30px;
	padding-top:20px;
	font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	color:#FFFFFF;
	font-size:20px;
}

.loader{
	width:90px;
	height:90px;
	background-color:#FFFFFF;
	overflow:hidden;
	display:block;
	-moz-border-radius:100%;
	-webkit-border-radius:100%;
	border-radius:100%;
	border:3px solid #5dba9d;
	position:relative;
	margin:15px auto;
  z-index:1;
}

.element-animation{
  animation:animationFrames linear 3s;
  animation-iteration-count: infinite;
  animation-fill-mode: forwards; /*when the spec is finished*/
  -webkit-animation: animationFrames linear 3s;
  -webkit-animation-iteration-count: infinite;
  -webkit-animation-fill-mode:forwards; /*Chrome 16+, Safari 4+*/ 
  -moz-animation: animationFrames linear 3s;
  -moz-animation-iteration-count: infinite;
  -moz-animation-fill-mode:forwards; /*FF 5+*/
  -o-animation: animationFrames linear 3s;
  -o-animation-iteration-count: infinite;
  -o-animation-fill-mode:forwards; /*Not implemented yet*/
  -ms-animation: animationFrames linear 3s;
  -ms-animation-iteration-count: infinite;
  -ms-animation-fill-mode:forwards; /*IE 10+*/
}


@keyframes animationFrames{
  0% {
    transform: translate(-1160px,15px)  ;
  }
  100% {
    transform: translate(0px,15px)  ;
  }
}

@-moz-keyframes animationFrames{
  0% {
    transform: translate(-1160px,15px)  ;
  }
  100% {
    transform: translate(0px,15px)  ;
  }
}

@-webkit-keyframes animationFrames {
  0% {
    transform: translate(-1160px,15px)  ;
  }
  100% {
    transform: translate(0px,15px)  ;
  }
}

@-o-keyframes animationFrames {
  0% {
    transform: translate(-1160px,15px)  ;
  }
  100% {
    transform: translate(-0px,15px)  ;
  }
}

@-ms-keyframes animationFrames {
  0% {
    transform: translate(-1160px,15px)  ;
  }
  100% {
    transform: translate(0px,15px)  ;
  }
}

</style>