
<?
	$domain ="http://lapexperience.com/"
?>
<script type="text/javascript">
  var directionsDisplay;
  
  var directionsService = new google.maps.DirectionsService();
  var map;
  var oldDirections = [];
  var currentDirections = null;
  var coordenadasInicio = {
			Lat: 0,
			Lng: 0
  };
  var markers=[];
  var contentString=[];
  var infoWindows=[];
  var mymarker;
  var routeActual;
  
  function initialize() {
  
    var myOptions = {
      zoom: 10,
      center: new google.maps.LatLng(-33.879,151.235),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  styles : [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"<? e($color) ?>"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
	   
    }
	
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	directionsDisplay = new google.maps.DirectionsRenderer({ 
			map: map ,
			polylineOptions:{strokeColor:'<? e($color) ?>'},
			markerOptions:{visible:false}
	}); 
	 

	$.post("http://lapexperience.com/frontend/ajax/loadwaypoints.php",{id:'<?php e($_GET['idevento'])?>',idioma:'<? e('es')?>', path_siteRoot: '<?php e($path) ?>'},function(data){
		
	
		
		var data = $.parseJSON(data);

        //alert(data[0].lat);
		
		var length = data.length;
			
		for (var i = 0; i < length; i++) {
		
		 
			//alert(data[i].id);
			/*markers[data[i].id] = new google.maps.Marker({
			   position: new google.maps.LatLng (data[i].lat,data[i].lng),
			   map: map,
			   icon:
			   		{
						path: fontawesome.markers.HOME,
						scale: 0.5,
						
						fillColor: '#f00',
						fillOpacity: 1
						
					}
				
			});
			contentString[data[i].id] = '<div style="width:295px">'+
			  '<div style="font-size:14px;font-weight:bold;">'+data[i].titulo+'</div>'+
			  '<div id="bodyContent">'+
			  '<img src="/beta/upload/info_eventos/waypoints/fotos/'+data[i].imagen+'">'+
			  '</div>'+
			  '</div>';
		
			  infowindow[data[i].id] = new google.maps.InfoWindow({
				  content: contentString[data[i].id]
			  });
			
			 
			  google.maps.event.addListener(markers[data[i].id], 'click', function() {
				infowindow[data[i].id].open(map,markers[data[i].id]);
			  });*/
			  
			  
			  if(i==0){
				  
				map.setCenter(new google.maps.LatLng (data[i].lat,data[i].lng));	  
			  }
			  
			  
			  
			    //var location = new google.maps.LatLng(earthquakes[i].geolat, earthquakes[i].geolong);
				var marker = new google.maps.Marker({
					position : new google.maps.LatLng (data[i].lat,data[i].lng),
					map : map,
					/*icon:
			   		{
						path: fontawesome.markers.HOME,
						scale: 0.5,
						
						fillColor: '#f00',
						fillOpacity: 1
						
					},*/
					icon:
			   		{
						url: '<? echo $domain ?>'+data[i].icono
						
						
					},
					infoWindowIndex : i //<---Thats the extra attribute
				});
				if(data[i].pano){
					var krpanolink='<a href="javascript:obrePano('+data[i].id+')"><img src="<?php e($domain) ?>/backoffice/upload/info_eventos/waypoints/fotos/'+data[i].imagen+'" width="265px"></a>';	
				}else{
					var krpanolink='<img src="<?php e($domain) ?>/backoffice/upload/info_eventos/waypoints/fotos/'+data[i].imagen+'" width="265px">';
				}	
				var content = '<div style="width:265px">'+
				  '<div style="font-size:16px;font-weight:bold;color:#46434e">'+data[i].titulo+'</div>'+
				  '<div id="bodyContent">'+
				  '<p style="margin-top:0px">'+data[i].direccion+'</p>'+
				  
				  ' '+krpanolink+' '+
				  '</div>'+
				  '</div>';
				var infoWindow = new google.maps.InfoWindow({
					content : content
				});
		
				google.maps.event.addListener(marker, 'click', 
					function(event)
					{
						//map.panTo(event.latLng);
						//map.setZoom(5);
						infoWindows[this.infoWindowIndex].open(map, this);
					}
				);
		
				infoWindows.push(infoWindow);
				markers.push(marker);			
			
		 }
	});
	
	
	
	
	
	
	
  }


  $( document ).ready(function() {
	  initialize();
	  var posicionReal = $("#footer").offset();
	  var heightMap=window.innerHeight-55;

	  
	  $("#map_canvas").css("height", heightMap);
	  $("#map_canvas").css("width", window.innerWidth);
	   $(".content_directions").css("height", heightMap);
	  
	  
	 	
		
		function localizacion (posicion) {
			coordenadasInicio = {
				Lat: posicion.coords.latitude,
				Lng: posicion.coords.longitude
			}
			
			map.setCenter(new google.maps.LatLng(coordenadasInicio.Lat, coordenadasInicio.Lng));
			mymarker = new google.maps.Marker({
			   position: new google.maps.LatLng (coordenadasInicio.Lat,coordenadasInicio.Lng),
			   map: map,
			   icon:
			   		{
						url: "<? echo $domain ?>/frontend/images/icon_miposition.png",
						anchor: new google.maps.Point(23,23)
						
					}
				
			});
			
		}
		function errores(error) {
		
		}
	    if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(localizacion,errores);
		} else {
			
		}
		
		
		
  });
</script>
<style>

ul.waypoints{
	float:left;
	width:200px;
	margin:0;
	padding:0;
	
	
}
ul.waypoints li{
	position:relative;
	float:left;
	width:130px;
	padding:10px 40px 10px 26px !important;
	list-style:none;
	font-size:13px;
	background:#514e59;
	border-top:#5b5962 1px solid;
	border-bottom:#38363f 1px solid;
	border-left: 4px solid #E83737;
	color:#fff;
	margin-left:3px !important;
	
	
}
ul.waypoints li .go{
	position:absolute;
	top:8px;
	right:5px;
	width:42px;
	height:22px;
	background:url(<?php e($path) ?>img/botogo.jpg) bottom center;
	cursor:pointer;
	
	
}
ul.waypoints li .go:hover{
	/*background:url(/beta/img/botogo.jpg) top center;*/
}
ul.waypoints li .ongo{
	background:url(<?php e($path) ?>img/botogo.jpg) top center;
	
	
}
.withpaneldirecctions{
	margin-left:-500px;
	padding-left:500px;
	
}
.content_directions{
	position:relative;
	float:left;
	width:300px; 
	display:none;
	overflow:auto;
	background:#FFF;
	z-index:1000;
	font-size:12px;
	
}



</style>

<div id="map_canvas" style="float:left;width:100%; ">map</div>