<?php

  $lang['es'][1] = "Estáticas";
  $lang['en'][1] = "Static";
  $lang['it'][1] = "Statica";
  $lang['fr'][1] = "Statique";
  $lang['gr'][1] = "Statisch";
  $lang['po'][1] = "Estático";


  $lang['es'][2] = "Dinamicas";
  $lang['en'][2] = "Dinamics";
  $lang['it'][2] = "Dinamicas";
  $lang['fr'][2] = "Dynamiques";
  $lang['gr'][2] = "Dynamiks";
  $lang['po'][2] = "Dinâmicas";


  $lang['es'][3] = "Clima";
  $lang['en'][3] = "Weather";
  $lang['it'][3] = "Tempo";
  $lang['fr'][3] = "Temps";
  $lang['gr'][3] = "Wetter";
  $lang['po'][3] = "Tempo";



?>
<script language="javascript">
  $( document ).ready(function() {
      $( ".imagen" ).click(function() {
        $('.loader2').css('display','')
         var url = $(this).attr('rel')
         app.openFile(url);
      });
                                    
  });
</script>

<div class="navigation-bar" style="position:fixed; bottom:50px;  background-color:#222222; width:100%; z-index:9">
  <div class="button-bar__button bdinamicas" onclick="app.changeGallery(1)">
   <div class="button-barcontenido-a-centrar" style="color:#ACACAC">  <i class="fa fa-picture-o"></i>
    <?php echo $lang[$_GET['lang']][2] ?> </div>
  </div>
  
  <div class="button-bar__button2 bestaticas" style="background-color:#777777; border-left: 0px solid #C2C2C2" onclick="app.changeGallery(0)">
    <div class="button-barcontenido-a-centrar" style="color:#ACACAC">
    <i class="fa fa-picture-o"></i>
    <?php echo $lang[$_GET['lang']][1] ?></div>
  </div>
</div>

<div class="Divdinamicas" style=" margin-left:1% ">
  <div class="my-simple-gallery"  style="margin-left:8%" align="center" >
<?php

$galeriaQuery = Database::query('SELECT * FROM imagenes WHERE idevento="'.$_GET['idevento'].'" and tipo=1');
for($i=0;$i<count($galeriaQuery);$i++)
{

?>
  <figure itemprop="associatedMedia" >
    <div class="share btnImg" rel="http://seatexperience.net/backoffice/upload/info_eventos/dinamicas/<? e($galeriaQuery[$i]['file']) ?>"><div id="Recomendar" style="z-index:8">
    <i class="fa fa-share-alt"> </i></div></div>

    <div class="btnImg" onclick="app.download('http://seatexperience.net/backoffice/upload/info_eventos/dinamicas/<? e($galeriaQuery[$i]['file']) ?>','<? e($galeriaQuery[$i]['file']) ?>')"><div id="downloadBt" style="z-index:8">
    <i class="fa fa-cloud-download"> </i></div></div>

    <div class="imagen"  rel="http://ws.seatexperience.net/redimensionadorImg.php?x=500&y=500&image=http://seatexperience.net/backoffice/upload/info_eventos/dinamicas/tumb_<? e($galeriaQuery[$i]['file']) ?>"><img src="http://seatexperience.net/backoffice/upload/info_eventos/dinamicas/tumb_<? e($galeriaQuery[$i]['file']) ?>"  /></div>        
  </figure>
  
  
  <? } ?>
  </div>
</div>



<div class="Divestaticas" style="display:none; margin-left:7% ">
  <div class="my-simple-gallery"   align="center" >

<?php

      $galeriaQuery = Database::query('SELECT * FROM imagenes WHERE idevento="'.$_GET['idevento'].'" and tipo=2');
      for($i=0;$i
      <count($galeriaQuery);$i++)
      {

      ?>

  <figure itemprop="associatedMedia" >
    <div class="share btnImg" rel="http://seatexperience.net/backoffice/upload/info_eventos/estaticas/<? e($galeriaQuery[$i]['file']) ?>"><div id="Recomendar" style="z-index:8">
    <i class="fa fa-share-alt"> </i></div></div>

    <div class="btnImg" onclick="app.download('http://seatexperience.net/backoffice/upload/info_eventos/estaticas/<? e($galeriaQuery[$i]['file']) ?>','<? e($galeriaQuery[$i]['file']) ?>')"><div id="downloadBt" style="z-index:8">
    <i class="fa fa-cloud-download"> </i></div></div>

    <div class="imagen"  rel="http://ws.seatexperience.net/redimensionadorImg.php?x=500&y=500&image=http://seatexperience.net/backoffice/upload/info_eventos/estaticas/tumb_<? e($galeriaQuery[$i]['file']) ?>"><img src="http://seatexperience.net/backoffice/upload/info_eventos/estaticas/tumb_<? e($galeriaQuery[$i]['file']) ?>"  /></div>        
  </figure>

  <? } ?>
  </div>
</div>

