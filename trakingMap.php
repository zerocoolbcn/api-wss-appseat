<?php
		
		$m = new Mongo();

		$db = $m->lap;

		$tabla = $db->trakingData;

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<div id="directions_panel" class="directions_panel" style="overflow:auto; background-color:#fff;  height:280px;">

</div>

<script type="text/javascript">
	
	var directionsDisplay;

	var directionsService = new google.maps.DirectionsService();
	var map;
	var oldDirections = [];
	var currentDirections = null;
	
	var coordenadasInicio = {
		Lat: 0,
		Lng: 0
	};
	
	var markers=[];
	var contentString=[];
	var infoWindows=[];
	var mymarker;
	var routeActual;
   
  function initialize() {
  
    var myOptions = {
      zoom: 8,
      center: new google.maps.LatLng(-33.879,151.235),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  styles : [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#EF0000"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#EF0000"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#EF0000"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#EF0000"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]

      }
	
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	directionsDisplay = new google.maps.DirectionsRenderer({ 
			map: map ,
			polylineOptions:{strokeColor:window.color},
			markerOptions:{visible:false}
	}); 
	 
	directionsDisplay.setPanel(document.getElementById("directions_panel"));
	
	
	$.post("http://ws.lapexperience.com/loadtraking.php",{id:'553d3c955d802',roadid:'1', path_siteRoot: 'http://www.lapexperience.com/'},function(data){
		
		var data = $.parseJSON(data);
		console.log(data)
		var length = data.length;

		var waypts = [];
		
		for (var i = 1; i < length-1; i++) {
			 
			 waypts.push({
				  location:new google.maps.LatLng (data[i].lat,data[i].lng),
				  stopover:false
			  });
				  
				if(i==0){
					  
					map.setCenter(new google.maps.LatLng (data[i].lat,data[i].lng));	  
				}
		 }
		 
		 	directionsDisplay.setMap(map);
			
		  
			var start = new google.maps.LatLng(data[0].lat, data[0].lng);
			var end = new google.maps.LatLng(data[length-1].lat, data[length-1].lng);

			var request = {
				origin:start,
				destination:end,
				waypoints: waypts,

				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};
			

			directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			//directionsRenderer.setDirections(response);
			
	
			  // Box around the overview path of the first route
			  //var path = response.routes[0].overview_path;
				}
			});
		 
		 
	});

	
	
	
  }


  function calcRoute(lat, lng, id) {
	  //alert("calculando");
	  
	if(routeActual==id){
		$('.go').removeClass('ongo');
		routeActual=0;
		hidePanelDirections();
		
			directionsDisplay.setMap(null);
			//directionsDisplay = null;
		
	}else{
		directionsDisplay.setMap(map);
		routeActual=id;
	  	$('.go').removeClass('ongo');
	  	$('#go'+id).addClass('ongo');
	  
    	var start = new google.maps.LatLng(coordenadasInicio.Lat, coordenadasInicio.Lng);
    	var end = new google.maps.LatLng(lat, lng);
    	var request = {
			origin:start,
			destination:end,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
    	};
   		directionsService.route(request, function(response, status) {
      		if (status == google.maps.DirectionsStatus.OK) {
        	directionsDisplay.setDirections(response);
		//directionsRenderer.setDirections(response);
        
		
	
          // Box around the overview path of the first route
          //var path = response.routes[0].overview_path;
      		}
    	});
	}
  }
  $( document ).ready(function() {

	  initialize();
	  var posicionReal = $("#footer").offset();
	 


	  
	  
	 	
		
		function localizacion (posicion) {
			coordenadasInicio = {
				Lat: posicion.coords.latitude,
				Lng: posicion.coords.longitude
			}
			//alert(coordenadas.Lat);alert(coordenadas.Lng);
			map.setCenter(new google.maps.LatLng(coordenadasInicio.Lat, coordenadasInicio.Lng));
			mymarker = new google.maps.Marker({
			   position: new google.maps.LatLng (coordenadasInicio.Lat,coordenadasInicio.Lng),
			   map: map,
			   icon:
			   		{
						url: "http://www.lapexperience.com/frontend/images/icon_miposition.png",
						anchor: new google.maps.Point(23,23)
						
					}
				
			});
			
		}
		function errores(error) {
		
		}
	    if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(localizacion,errores);
		} else {
			
		}
		
		

  });
</script>
<div id="map_canvas" style="float:left;width:100%; height:520px;  position:absolute">map</div>
