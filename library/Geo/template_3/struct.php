<?php

	$image = $this->getCountryFlag(175);
				
	$country = $info->getCountryName();
	if( !$country )
		$country = "Unknown";
	
	$city = $info->getCityName();
	if( !$city )
		$city = "Unknown";
	
	$region = $info->getRegionName();
	if( !$region )
		$region = "Unknown";
	
	$zip = $info->getZipCode();
	if( !$zip )
		$zip = "Unknown";
	
	$timezone = $info->getTimezone();
	if( !$timezone )
		$timezone = "Unknown";
	else
	{
		$sign = substr($timezone, 0, 1);
		
		if( $sign == '-' )
		{
			$timezone = substr($timezone, 1);
			$timezone = "-" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
		else
		{
			$timezone =  "+" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
	}
?><link rel="stylesheet" type="text/css" href="<?php echo  self::SOURCE_URL ; ?>template_3/style.css" />
<div class="geoiploc_container">
 <div class="sub sub_1">
 <table border="0" cellspacing="2" cellpadding="2">
   <tr>
     <td width="170">
        <?php echo $image; ?>
     </td>
     <td>
        <span class="label">IP Address</span>
        <span class="ip_address"><?php echo  $this->getIP(); ?></span>
        
        <br />
        <span class="label">Country Name</span>
        <span class="country_name"><?php echo  $country; ?></span>
        
        
        <br />
        <span class="label">City Name / Region Name</span>
        <span class="city_region"><?php echo  $city; ?> / <?php echo  $region; ?></span>
        
     </td>
   </tr>
 </table>
 </div>
 
 <div class="sub sub_2">
   <?php $this->getLocationMap(282, 200, $map_zoom_level, true, false); ?>
 </div>
</div>