<?php

	/** This version of class is with IP Caching Module **/
	
	// GeoIPLocator interface import
	require("GeoIPLocator.interface.php");
	
	// IPAddress model class
	require("IPAddress.class.php");
	
	// Including MySQL connect information
	include("mysql_config.php");
	
	/**
	 * IP Locator Class 
	 *
	 * @class GeoIPLocator
	 * @author Arlind Nushi
	 * @version 2.0 plus
	 */
	
	class GeoIPLocator implements GeoIPLocatorInterface
	{		
		/** @var $IP - IPv4 Address */
		private $IP;
		
		/** @var $information - IPAdress class */
		private $information;
		
		/** @var $exec_time - Execution time in ms */
		private $exec_time;
		
		/** @var $caching - Enable caching or not / By default: true */
		private $caching = true;
		
		
		/**
		 * Constructor
		 *
		 * @param $ip - IPv4 Address
		 */
		 
		public function __construct($ip, $enable_caching = true)
		{
			if( !$ip )
				$ip = $_SERVER['REMOTE_ADDR'];
			
			if( !$enable_caching )
				$this->caching = false;
				
			$this->setIP($ip);
			
			if( $this->getIP() )
			{
				$this->lookup();
			}
		}
		
		
		/**
		 * Set IP
		 *
		 * @param $ip - IPv4 format
		 */
		
		public function setIP($ip)
		{
			$ip_portion = "(1[0-9][0-9]|2[0-5][0-5]|2[0-4][0-9]|[1-9][0-9]|[0-9])";
			$ip_pattern = "/^$ip_portion\.$ip_portion\.$ip_portion\.$ip_portion$/";
			
			if( preg_match($ip_pattern, $ip) )
			{
				$this->exec_time = (float) array_sum(explode(' ',microtime())); 
				$this->IP = $ip;
			}
			else
			{
				echo "GeoIPLocator::setIP($ip)::error:: <b>Invalid format of IPv4 address</b>";
			}
		}
		
		
		/**
		 * Get Current IP
		 *
		 * @return IPv4 address format
		 */
		
		public function getIP()
		{
			return $this->IP;
		}
		
		
		/**
		 * Lookup for IP address
		 *
		 * @return void
		 */
		
		public function lookup()
		{
			$caching = $this->caching;
			
			
			if( $caching )
			{
				$ip_cache_connection = mysql_pconnect(GEOIPLOC_MYSQL_HOST, GEOIPLOC_MYSQL_USER, GEOIPLOC_MYSQL_PASS) or die(mysql_error());
				mysql_select_db(GEOIPLOC_MYSQL_DATABASE, $ip_cache_connection) or die(mysql_error());
			}
			
			if( $caching )
			{
				$ip = mysql_real_escape_string($this->getIP());
				$q = sprintf("SELECT * FROM `ip_cache` WHERE IP = '%s'", $ip);
				$check_q = mysql_query($q, $ip_cache_connection) or die(mysql_error());
				
				if( mysql_num_rows($check_q) )
				{
					$information = mysql_fetch_array($check_q);
					
					$ip				= $information['IP'];
					$status			= $information['STATUS'];
					$countrycode 	= $information['COUNTRY_CODE'];
					$countryname	= $information['COUNTRY_NAME'];
					$regioncode		= $information['REGION_CODE'];
					$regionname		= $information['REGION_NAME'];
					$cityname		= $information['CITY_NAME'];
					$zipcode		= $information['ZIP_CODE'];
					$latitude		= $information['LATITUDE'];
					$longitude		= $information['LONGITUDE'];
					$timezone		= $information['TIMEZONE'];
					
					$ip = new IPAddress($ip, $status, $countrycode, $countryname, $regioncode, $regionname, $cityname, $zipcode, $latitude, $longitude, $timezone);
					
					$this->information = $ip;
					$this->exec_time = (float)array_sum(explode(' ',microtime())) - $this->exec_time;
					mysql_close($ip_cache_connection);
					return true;					
				}
				
			}
			
			$url_string = self::API_URL;
			$url_string = str_replace(array("#API_KEY#", "#REMOTE_ADDR#"), array(self::API_KEY, $this->getIP()), $url_string);
			
			#$contents = file_get_contents($url_string); OLD METHOD - slower
			
			/** New Method - Faster **/
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$contents = curl_exec($ch);
			curl_close($ch);
			
			// Match IP
			curl_close($ch);
			
			// Match IP
			$xml = simplexml_load_string($contents);
			
			$xml_ip = $xml->xpath("//Ip");
			$xml_st = $xml->xpath("//Status");
			$xml_cc = $xml->xpath("//CountryCode");
			$xml_cn = $xml->xpath("//CountryName");
			$xml_rc = $xml->xpath("//RegionCode");
			$xml_rn = $xml->xpath("//RegionName");
			$xml_ct = $xml->xpath("//City");
			$xml_zc = $xml->xpath("//ZipPostalCode");
			$xml_la = $xml->xpath("//Latitude");
			$xml_lo = $xml->xpath("//Longitude");
			$xml_tz = $xml->xpath("//Timezone");
			
			$ip				= $xml_ip[0];
			$status			= $xml_st[0];
			$countrycode	= $xml_cc[0];
			$countryname	= $xml_cn[0];
			$regioncode		= $xml_rc[0];
			$regionname		= $xml_rn[0];
			$cityname		= $xml_ct[0];
			$zipcode		= $xml_zc[0];
			$latitude		= $xml_la[0];
			$longitude		= $xml_lo[0];
			$timezone		= $xml_tz[0];
			
						
			if( $caching )
			{
				$insert_query = sprintf("INSERT INTO `ip_cache`(`IP`,`STATUS`,`COUNTRY_CODE`,`COUNTRY_NAME`,`REGION_CODE`,`REGION_NAME`,`CITY_NAME`,`ZIP_CODE`,`LATITUDE`,`LONGITUDE`,`TIMEZONE`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", $ip, $status, $countrycode, $countryname, $regioncode, $regionname, $cityname, $zipcode, $latitude, $longitude, $timezone);
				mysql_query($insert_query, $ip_cache_connection);
				mysql_close($ip_cache_connection);
			}
			
			$ip = new IPAddress($ip, $status, $countrycode, $countryname, $regioncode, $regionname, $cityname, $zipcode, $latitude, $longitude, $timezone);
			
			$this->information = $ip;
			$this->exec_time = (float)array_sum(explode(' ',microtime())) - $this->exec_time;
		}
		
		
		/**
		 * Get IP Information
		 *
		 * @return IPAddress
		 */
		
		public function getIPInfo()
		{
			$information = $this->information;
			
			return $information;
		}
		
		
		/**
		 * Get Location
		 * 
		 * @param $width - 200 pixels default
		 * @param $height - 140 pixels default
		 * @param $zoom_level - 4 default
		 *
		 * @return JavaScript Code for Google MAP using Google API
		 */
		
		public function getLocationMap($width = 200, $height = 140, $zoom_level = 4, $dragable = false, $controller = false)
		{
			$country = $this->getIPInfo()->getCountryName();
			$region = $this->getIPInfo()->getRegionName();
			$city = $this->getIPInfo()->getCityName();
			
			$title = "";
				
			if( $city )
				$title .= ", $city";
				
			if( $region )
				$title .= ", $region";
				
			if( $country )
				$title .= ", $country";				
			
			$title = substr($title, 2);
			
			$latitude = $this->getIPInfo()->getLatitude();
			$longitude = $this->getIPInfo()->getLongitude();
					
			$api = self::GOOGLE_API;
			
			?><script type="text/javascript" src="http://www.google.com/jsapi?key=<?php echo $api; ?>"></script>
<script type="text/javascript">
  google.load("maps", "2.x");
   
  function initialize()
  {
	var map = new google.maps.Map2(document.getElementById("map"));
	
	var point = new GPoint( <?php echo $longitude; ?>, <?php echo $latitude; ?>);
	var marker = new GMarker(point, {"title" : "<?php echo $title; ?>"});
	
	<?php if( !$dragable ){ ?>
	map.disableDragging();<?php } ?>
	
	map.setCenter(new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>), <?php echo $zoom_level; ?>);
	
	<?php if( $controller ){ ?>
	map.addControl(new GLargeMapControl());
	<?php } ?>
	
	map.addOverlay(marker);
  }
  google.setOnLoadCallback(initialize);
</script>
<div id="map" style="width:<?php echo $width; ?>px; height:<?php echo $height; ?>px;" class="geoiplocator_map"></div><?php
		}
		
		
		/**
		 * Get Image location for a specified country
		 *
		 * @return Image Location (url + relative location)
		 */
		 
		public function getCountryFlag($default_size = 256, $return_url = false)
		{
			$country = array("afghanistan" => true, "albania" => true, "algeria" => true, "american_samoa" => true, "andorra" => true, "angola" => true, "anguilla" => true, "antigua_and_barbuda" => true, "argentina" => true, "armenia" => true, "aruba" => true, "australia" => true, "austria" => true, "azerbaijan" => true, "bahrain" => true, "bangladesh" => true, "barbados" => true, "belarus" => true, "belgium" => true, "belize" => true, "benin" => true, "bermuda" => true, "bhutan" => true, "bolivia" => true, "bosnia_and_herzegovina" => true, "bostswana" => true, "bouvet_island" => true, "brazil" => true, "british_indian_ocean_territory" => true, "british_virgin_islands" => true, "brunei" => true, "bulgaria" => true, "burkina_faso" => true, "burma" => true, "burundi" => true, "cambodia" => true, "cameroon" => true, "canada" => true, "cape_verde" => true, "cayman_islands" => true, "central_african_republic" => true, "chad" => true, "chile" => true, "china" => true, "christmas_islands" => true, "cocos_(keeling)_islands" => true, "colombia" => true, "comoros" => true, "cook_islands" => true, "costa_rica" => true, "cote_d'ivoire" => true, "croatia" => true, "cuba" => true, "cyprus" => true, "czech_republic" => true, "democratic_republic_of_the_congo" => true, "denmark" => true, "djibouti" => true, "dominica" => true, "dominican_republic" => true, "ecuador" => true, "egypt" => true, "el_salvador" => true, "england" => true, "equatorial_guinea" => true, "eritrea" => true, "estonia" => true, "ethiopia" => true, "europe" => true, "european_union" => true, "falkland_islands_(islas_malvinas)" => true, "faroe_islands" => true, "fiji" => true, "finland" => true, "france" => true, "french_poynesia" => true, "gabon" => true, "georgia" => true, "germany" => true, "ghana" => true, "gibraltar" => true, "greece" => true, "greenland" => true, "grenada" => true, "guam" => true, "guatemala" => true, "guernsey" => true, "guinea-blissau" => true, "guinea" => true, "guyana" => true, "haiti" => true, "holy_see_(vatican_city)" => true, "honduras" => true, "hong_kong" => true, "hungary" => true, "iceland" => true, "india" => true, "indonesia" => true, "iran" => true, "iraq" => true, "ireland" => true, "isle_of_man" => true, "israel" => true, "italy" => true, "jamaica" => true, "japan" => true, "jordan" => true, "kazahstan" => true, "kenya" => true, "kiribati" => true, "kosovo" => true, "kuwait" => true, "kyrgyzstan" => true, "laos" => true, "latvia" => true, "lebanon" => true, "lesotho" => true, "liberia" => true, "libya" => true, "liechtenstein" => true, "lithuania" => true, "luxembourg" => true, "macau" => true, "macedonia" => true, "madagascar" => true, "malawi" => true, "malaysia" => true, "maldives" => true, "mali" => true, "malta" => true, "marschal_islands" => true, "mauritania" => true, "mauritius" => true, "mayotte" => true, "mexico" => true, "micronesia" => true, "moldavia" => true, "monaco" => true, "mongolia" => true, "montenegro" => true, "montserrat" => true, "morocco" => true, "mozambique" => true, "namibia" => true, "nauru" => true, "nepal" => true, "netherlands" => true, "netherlands_anthilles" => true, "new_zealand" => true, "nicaragua" => true, "niger" => true, "nigeria" => true, "niue" => true, "norfolk_island" => true, "north_korea" => true, "northern_mariana_islands" => true, "norway" => true, "oman" => true, "pakistan" => true, "palau" => true, "palestine" => true, "panama" => true, "papua_new_guinea" => true, "paraguay" => true, "peru" => true, "philippines" => true, "pitcairn_islands" => true, "poland" => true, "portugal" => true, "puerto_rico" => true, "quatar" => true, "republic_of_the_congo" => true, "romania" => true, "russia" => true, "russian_federation" => true, "rwanda" => true, "saint_helena" => true, "saint_kitts_and_nevis" => true, "saint_lucia" => true, "saint_pierre_and_miquelon" => true, "saint_vincent" => true, "samoa" => true, "san_marino" => true, "sao_tome_and_principe" => true, "saudi_arabia" => true, "scotland" => true, "senegal" => true, "serbia" => true, "seychelles" => true, "sierra_leone" => true, "singapore" => true, "slovakia" => true, "slovenia" => true, "solomon_islands" => true, "somalia" => true, "south_africa" => true, "south_georgia_and_the_south_sandwitch_islands" => true, "south_korea" => true, "spain" => true, "sri_lanka" => true, "sudan" => true, "suriname" => true, "swaziland" => true, "sweden" => true, "switzerland" => true, "syria" => true, "taiwan" => true, "tajikistan" => true, "tanzania" => true, "thailand" => true, "the_bahamas" => true, "the_gambia" => true, "tibet" => true, "timor-leste" => true, "togo" => true, "tonga" => true, "trinidad_and_tobago" => true, "tunisia" => true, "turkey" => true, "turkmenistan" => true, "turks_and_caicos_islands" => true, "tuvalu" => true, "uganda" => true, "ukraine" => true, "united_arab_emirates" => true, "united_kingdom" => true, "united_states" => true, "uruguay" => true, "uzbekistan" => true, "vanuatu" => true, "venezuela" => true, "vietnam" => true, "virgin_islands" => true, "wales" => true, "wallis_and_futuna" => true, "yugoslavia" => true, "yemen" => true, "zambia" => true, "zimbabwe");
			
			if( is_numeric($default_size) && $default_size > 0 && $default_size < 256 )
				$def_size = $default_size;
			else
				$def_size = 256;
			
			$city_name = strtolower($this->getIPInfo()->getCityName());
			$city_name = str_replace(" ", "_", $city_name);
			
			$region_name = strtolower($this->getIPInfo()->getRegionName());
			$region_name = str_replace(" ", "_", $region_name);
			
			$country_name = strtolower($this->getIPInfo()->getCountryName());
			$country_name = str_replace(" ", "_", $country_name);
						
			$src = "unknown.png";
			
			if( $country[ $country_name ] )
			{
				$src = "$country_name.png";
			}
			else
			if( $country[ $region_name ] )
			{
				$src = "$region_name.png";
			}
			else
			if( $country[ $city_name ] )
			{
				$src = "$city_name.png";
			}
			
			if( $return_url )
			{
				return self::SOURCE_URL.'flags_png/'.$src;
			}
			
			$img = '<img src="'.self::SOURCE_URL.'flags_png/'.$src.'" width="'.$def_size.'" height="'.$def_size.'" alt="'.$this->getIPInfo()->getCountryName().'" />';
			
			return $img;
		}
		
		
		/**
		 * Parse IP details in HTML mode
		 *
		 * @param $template - Template name
		 */
		
		public function showDetails($template, $map_zoom_level = 7)
		{
			$info = $this->getIPInfo();
			
			switch( $template )
			{
				case "template_2":
					include("template_2/struct.php");					
					break;
				
				case "template_3":
					include("template_3/struct.php");
					break;
				
				case "template_4":
					include("template_4/struct.php");
					break;
	
				default:
					include("template_1/struct.php");
			}
		}
		
		/**
		 * Get execution time
		 *
		 * @return Time in milliseconds
		 */
		
		public function execTime()
		{
			return $this->exec_time;
		}
		
		
		/**
		 * Disable IP Caching
		 *
		 * @return void
		 */
		
		public function disableCaching()
		{
			$this->caching = false;
		}
	}
		
?>