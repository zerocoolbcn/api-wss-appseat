<?php
	
	/**
	 * IP Address model
	 *
	 * @class IPAddress
	 * @author Arlind Nushi
	 * @version 1.0
	 */
	
	class IPAddress
	{
		private $ip;
		private $status;
		private $countrycode;
		private $countryname;
		private $regioncode;
		private $regionname;
		private $cityname;
		private $zipcode;
		private $latitude;
		private $longitude;
		private $timezone;
		
		
		public function __construct($ip, $status, $countrycode, $countryname, $regioncode, $regionname, $cityname, $zipcode, $latitude, $longitude, $timezone)
		{
			$this->setIp($ip);
			$this->setStatus($status);
			$this->setCountryCode($countrycode);
			$this->setCountryName($countryname);
			$this->setRegionCode($regioncode);
			$this->setRegionName($regionname);
			$this->setCityName($cityname);
			$this->setZipCode($zipcode);
			$this->setLatitude($latitude);
			$this->setLongitude($longitude);
			$this->setTimezone($timezone);
		}
		
		public function getIp()
		{
			return $this->ip;
		}
		
	
		public function setIp($ip)
		{
			$this->ip = $ip;
		}
		
	
		public function getStatus()
		{
			return $this->status;
		}
		
	
		public function setStatus($status)
		{
			$this->status = strstr($status, "OK") ? true : false;;
		}
		
	
		public function getCountryCode()
		{
			return $this->countrycode;
		}
		
	
		public function setCountryCode($countrycode)
		{
			$this->countrycode = $countrycode;
		}
		
	
		public function getCountryName()
		{
			return $this->countryname;
		}
		
	
		public function setCountryName($countryname)
		{
			$this->countryname = $countryname;
		}
	
	
		public function getRegionCode()
		{
			return $this->regioncode;
		}
	
	
		public function setRegionCode($regioncode)
		{
			$this->regioncode = $regioncode;
		}
	
	
		public function getRegionName()
		{
			return $this->regionname;
		}
	
	
		public function setRegionName($regionname)
		{
			$this->regionname = $regionname;
		}
		
	
		public function getCityName()
		{
			return $this->cityname;
		}
	
	
		public function setCityName($cityname)
		{
			$this->cityname = $cityname;
		}
		
	
		public function getZipCode()
		{
			return $this->zipcode;
		}
		
	
		public function setZipCode($zipcode)
		{
			$this->zipcode = $zipcode;
		}
		
	
		public function getLatitude()
		{
			return $this->latitude;
		}
		
	
		public function setLatitude($latitude)
		{
			$this->latitude = $latitude;
		}
	
		public function getLongitude()
		{
			return $this->longitude;
		}
	
	
		public function setLongitude($longitude)
		{
			$this->longitude = $longitude;
		}
	
	
		public function getTimezone()
		{
			return $this->timezone;
		}
	
	
		public function setTimezone($timezone)
		{
			$this->timezone = $timezone;
		}
		
		/**
		 * If your PHP version is >= 5 then you can use the method below for SET/GET
		 
		public function __call($func, $args)
		{
			switch( $func )
			{		
				case "setIp":
					$this->ip = $args[0];
					break;
					
				case "setStatus":
					$this->status = strstr($args[0], "OK") ? true : false;
					break;
					
				case "setCountryCode":
					$this->countrycode = $args[0];
					break;
					
				case "setCountryName":
					$this->countryname = $args[0];
					break;
					
				case "setRegionCode":
					$this->regioncode = $args[0];
					break;
					
				case "setRegionName":
					$this->regionname = $args[0];
					break;
					
				case "setCityName":
					$this->cityname = $args[0];
					break;
					
				case "setZipCode":
					$this->zipcode = $args[0];
					break;
					
				case "setLatitude":
					$this->latitude = $args[0];
					break;
					
				case "setLongitude":
					$this->longitude = $args[0];
					break;
					
				case "setTimezone":
					$this->timezone = $args[0];
					break;
				
						
				case "getIp":
					return $this->ip;
					break;
					
				case "getStatus":
					return $this->status;
					break;
					
				case "getCountryCode":
					return $this->countrycode;
					break;
					
				case "getCountryName":
					return $this->countryname;
					break;
					
				case "getRegionCode":
					return $this->regioncode;
					break;
					
				case "getRegionName":
					return $this->regionname;
					break;
					
				case "getCityName":
					return $this->cityname;
					break;
					
				case "getZipCode":
					return $this->zipcode;
					break;
					
				case "getLatitude":
					return $this->latitude;
					break;
					
				case "getLongitude":
					return $this->longitude;
					break;
					
				case "getTimezone":
					return $this->timezone;
					break;
			}
		} **/
		
		
		public function __toString()
		{
			if( !$this->getStatus() )
			{
				return "No Information About this IP";
			}
			
			return "IP: " . $this->getIp() . " - Status: ".($this->getStatus() ? "OK" : "Unknown")." - Country: " . $this->getCountryName();
		}
	}
?>