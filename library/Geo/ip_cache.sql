-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 25, 2010 at 02:49 PM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `geoip_locator`
--

-- --------------------------------------------------------

--
-- Table structure for table `ip_cache`
--

CREATE TABLE IF NOT EXISTS `ip_cache` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IP` varchar(50) NOT NULL,
  `COUNTRY_CODE` varchar(5) DEFAULT NULL,
  `COUNTRY_NAME` varchar(100) DEFAULT NULL,
  `REGION_CODE` varchar(5) DEFAULT NULL,
  `REGION_NAME` varchar(100) DEFAULT NULL,
  `CITY_NAME` varchar(100) DEFAULT NULL,
  `ZIP_CODE` varchar(50) DEFAULT NULL,
  `LATITUDE` varchar(50) DEFAULT NULL,
  `LONGITUDE` varchar(50) DEFAULT NULL,
  `TIMEZONE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ip_cache`
--

