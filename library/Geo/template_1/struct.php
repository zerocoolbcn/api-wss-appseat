<link rel="stylesheet" type="text/css" href="<?php echo  self::SOURCE_URL ; ?>template_1/style.css" />
<div class="geoiploc_container">
 <div class="ip_info">
<?php
	if( $info->getCountryName() == "Reserved" )
	{
		?>No Information about this ip<?php
		return;
	}
	else
	{
?>
  <div class="map">
   <?php
	$this->getLocationMap(286, 96, $map_zoom_level);
   ?>
  </div>

  
  <div class="info">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="100" valign="top"><?php echo  $this->getCountryFlag(96); ?></td>
		<td colspan="2" class="country_name"><table border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td colspan="2" valign="bottom" class="country_name"><?php echo  $info->getCountryName(); ?></td>
		  </tr>
          <?php if( $info->getRegionName() ){ ?>
		  <tr>
		    <td width="90">Region name: </td>
		    <td><?php echo  $info->getRegionName(); ?></td>
		  </tr>
          <?php } ?>
          <?php if( $info->getCityName() ){ ?>
		  <tr>
		    <td width="90">City name:</td>
		    <td><?php echo  $info->getCityName(); ?></td>
		  </tr>
          <?php } ?>
          <?php if( $info->getZipCode() ){ ?>
		  <tr>
		    <td width="90">Postal Code: </td>
		    <td><?php echo  $info->getZipCode(); ?></td>
		  </tr>
          <?php } ?>
		  </table></td>
		</tr>
      <?php if( $info->getRegionName() ) { ?>
	  <?php } ?>
      <?php if( $info->getCityName() ) { ?>
	  <?php } ?>
      <?php if( $info->getZipCode() ) { ?>
	  <?php } ?>
	  </table>
  </div>
 
 <div class="clear"></div>
</div>
<?php } ?>
</div>