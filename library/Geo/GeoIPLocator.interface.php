<?php

	interface GeoIPLocatorInterface
	{
		/** Constants **/
		
		/** @var API_KEY -  SET YOUR OWN REGISTERED API KEY - THIS IS MANDATORY VARIABLE, YOU NEED TO SET YOUR CORRECT API */
		const API_KEY = 'c4988fc24cce2ccc831239bed386e732a5c3e9c11ad03b4006d9946395085ed8';
		
		/** @var SOURCE-URL - URL of Source files for GeoIPLocation */
		const SOURCE_URL = 'http://ligotea.com/src/';//'http://yourdomainname.com/path/to/geoiplocator/class/';
		
		/** @var API-URL - Do not change this **/
		const API_URL = 'http://api.ipinfodb.com/v2/ip_query.php?key=#API_KEY#&ip=#REMOTE_ADDR#&timezone=true';
		
		/** @var GOOGLE-MAPS-API - Change API to your own GMAPS API **/
		const GOOGLE_API = 'ABQIAAAAUA5bHeOvCI6Y-1LpE8wgURQSnL5MFRJvhoz_IKyk-4q6VT1n0hQ2qO0tHUAzKiY6wHA1nNQaD5KjjQ';
		
		
		public function setIP($ip);
		
		public function getIP();
		
		public function lookup();
		
		public function getLocationMap($width = 200, $height = 140, $zoom_level = 4, $dragable = false, $controller = false);
		
		public function getCountryFlag();
		
		public function execTime();
		
		public function disableCaching();
	}
?>