<?php

	$image = $this->getCountryFlag(80);
				
	$country = $info->getCountryName();
	if( !$country )
		$country = "Unknown";
	
	$city = $info->getCityName();
	if( !$city )
		$city = "Unknown";
	
	$region = $info->getRegionName();
	if( !$region )
		$region = "Unknown";
	
	$zip = $info->getZipCode();
	if( !$zip )
		$zip = "Unknown";
	
	$timezone = $info->getTimezone();
	if( !$timezone )
		$timezone = "Unknown";
	else
	{
		$sign = substr($timezone, 0, 1);
		
		if( $sign == '-' )
		{
			$timezone = substr($timezone, 1);
			$timezone = "-" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
		else
		{
			$timezone =  "+" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
	}
?><link rel="stylesheet" type="text/css" href="<?php echo  self::SOURCE_URL ; ?>template_4/style.css" />
<div class="geoiploc_container">
  <table width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr>
      <td align="center" height="80"><?php echo  $image; ?></td>
    </tr>
    <tr>
      <td align="center" class="ip"><?php echo  $this->getIP(); ?>&nbsp;</td>
    </tr>
    <?php if( $country != "Unknown" ): ?>
    <tr>
      <td align="center" class="country"><?php echo  $country; ?>&nbsp;</td>
    </tr>
    <?php endif; ?>
    <?php if( $city != "Unknown" ): ?>
    <tr>
      <td align="center" class="city"><?php echo  $city; ?>&nbsp;</td>
    </tr>
    <?php endif; ?>
  </table>
 
</div>