<?php

	$image = $this->getCountryFlag(175);
				
	$country = $info->getCountryName();
	if( !$country )
		$country = "Unknown";
	
	$city = $info->getCityName();
	if( !$city )
		$city = "Unknown";
	
	$region = $info->getRegionName();
	if( !$region )
		$region = "Unknown";
	
	$zip = $info->getZipCode();
	if( !$zip )
		$zip = "Unknown";
	
	$timezone = $info->getTimezone();
	if( !$timezone )
		$timezone = "Unknown";
	else
	{
		$sign = substr($timezone, 0, 1);
		
		if( $sign == '-' )
		{
			$timezone = substr($timezone, 1);
			$timezone = "-" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
		else
		{
			$timezone =  "+" . ($timezone > 9 ? "" : "0") . $timezone . ":00";
		}
	}
?><link rel="stylesheet" type="text/css" href="<?php echo self::SOURCE_URL ; ?>template_2/style.css" />
<div class="geoiploc_container">
 <div class="sub">
   <div class="info">
     <table border="0" cellspacing="2" cellpadding="2">
       <tr>
         <td width="170">
         	<?php echo $image; ?>
         </td>
         <td>
         	<span class="label">IP Address</span>
            <span class="ip_address"><?php echo $this->getIP(); ?></span>
            
            <br />
         	<span class="label">Country Name</span>
            <span class="country_name"><?php echo $country; ?></span>
            
            
            <br />
         	<span class="label">City Name / Region Name</span>
            <span class="city_region"><?php echo $city; ?> / <?php echo $region; ?></span>
            
         </td>
       </tr>
     </table>
   </div>
   <div class="map">
     <table width="100%" border="0" cellspacing="2" cellpadding="2">
       <tr height="70">
         <td width="50%">
         	<span class="label">Zip Code</span>
            <span class="zip_code"><?php echo $zip; ?></span>
         </td>
         <td width="50%">
         	<span class="label">Timezone</span>
            <span class="timezone"><?php echo $timezone; ?></span>
         </td>
       </tr>
       <tr>
         <td colspan="2" align="center" class="map_env">
          <span>Location Map</span>
          <div class="map_border">
           <?php $this->getLocationMap(230, 85, $map_zoom_level, true, false); ?>
          </div>
         </td>
       </tr>
     </table>
     <br />

   </div>
 </div>
</div>