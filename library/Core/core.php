<?


class gianni{
	
	public function formateaexto($txt,$param=1)
	{
		if($param==1){
			$result =  ucfirst  (strtolower (utf8_encode( $txt ) ) );
		}
		if($param==2){
			$result = ucwords  (strtolower (utf8_encode( $txt ) ) );
		}
		if($param==3){
			$result = strtolower (utf8_encode( $txt ) );
		}
		return $result;
	}
}

function creadir($ruta)
{
	mkdir($ruta, 0777);
	return true;
}

function in_array_r($needle, $haystack, $strict = true) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
 
    return false;
}

function share($id,$contenido,$title)
{

	
	echo "<div class='social' id='".$id."'>
		  <div class='front card' id='".$id."'><i class='fa fa-share'></i></div>
		  <div class='back card' id='".$id."'>
			<a href='https://twitter.com/intent/tweet?url=".$contenido."&original_referer=".$contenido."' target='_blank' class='twitter card'><i class='fa fa-twitter'></i></a>
			<a href='http://www.facebook.com/sharer.php?u=".$contenido."&t=".$title."' target='_blank' class='facebook card'><i class='fa fa-facebook'></i></a>
			<a href='https://plus.google.com/share?url=".$contenido."&t=".$title."' target='_blank'  class='google card'><i class='fa fa-google-plus'></i></a>
			<div class='close card' id='".$id."'><i class='fa fa-times'></i></div>
		  </div>
		</div>";

}
/*
* Invertimos fecha de mysql retorna la funció _() del gettext
*/
function invertirFecha($i)
{
	// 2015-02-01
	$correjida = date("d-m-Y",strtotime($i));
	return $correjida;
}

/*
* Fa un echo del que ens retorna la funció _() del gettext
*/
function __($texto){
	echo _($texto);
}
/*
* Devuelve cadena codificada en 64 la funció _() del gettext
*/

function encode64($string)
{
	return base64_encode($string);
}
/*
* Devuelve cadena descodificada en 64 la funció _() del gettext
*/

function decode64($string)
{
	return base64_decode($string);
}
/*
*  Limpia el texto multidioma para devolverlo como url seo
*/

function _seo($str) {
	return url_neteja(_($str));
}


/*
* Fa un return del que ens retorna la funció _() del gettext, substituint les marques {x} per els paramtres passats
*/
function _p(){
	$p=func_get_args();
	$txt=_($p[0]);
	$params=array_slice(func_get_args(), 1);

	for($i=0; $i<count($params); $i++){
		$txt=str_replace("{".$i."}", $params[$i], $txt);
	}
	
	return $txt;
}
/*
* Devuelve error DEBUG php la funció _() del gettext, substituint les marques {x} per els paramtres passats
*/

function error($bol='false')
{

	if($bol){
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}else{
		error_reporting(0);
	}
}

/*
* Fa un echo del que ens retorna la funció _() del gettext, substituint les marques {x} per els paramtres passats
*/
function __p(){
	$p=func_get_args();
	$txt=_($p[0]);
	$params=array_slice(func_get_args(), 1);

	for($i=0; $i<count($params); $i++){
		$txt=str_replace("{".$i."}", $params[$i], $txt);
	}
	
	echo $txt;
}
/*
* Devuelve el bucle para for funció _() del gettext, substituint les marques {x} per els paramtres passats
*/


/*
* Imprime array para DEBUG funció _() del gettext, substituint les marques {x} per els paramtres passats
*/

function p($array)
{
	echo "<pre>";
	print_r($array);
}

/***************************************************************************************************************************************************************
* STRINGS
***************************************************************************************************************************************************************/

// Recuperamos el valor de REQUEST, get, post y session
function setVar($var_name='', $def='', $w_session=false){
	if(isset($_GET[$var_name]))	$var_name=$_GET[$var_name];
	else if(isset($_POST[$var_name])) $var_name=$_POST[$var_name];
	else if(isset($_SESSION[$var_name]) AND $w_session) $var_name=$_SESSION[$var_name];
	else $var_name=$def;
	
	return $var_name;
}

// Creamos variable de session y devolvemos variable
function setSesion($var_name=''){
	if(isset($_GET[$var_name]))	$_SESSION[$var_name]=$_GET[$var_name];
	else if(isset($_POST[$var_name])) $_SESSION[$var_name]=$_POST[$var_name];
	
	//return $_SESSION[$var_name];
}

/*
Escriu el test passat

Parametres:
  s -> El text que es vol escriure
*/
function e($s=''){
	echo $s;
}

/*
Filtra el text per guardar-lo a la bd

Parametres:
  s -> El text que es vol filtrar
*/
function f($s=''){
	if(get_magic_quotes_gpc()) {
		$t=stripslashes($s);
	} else {
		$t=$s;
	}
	
	return mysqli_real_escape_string($t);
}

/*
Ajusta el text per ser passat per URL

Parametres:
  s -> El text que es vol austar
*/
function u($s=''){
	return urlencode($s);
}

/*
Ajusta el text per ser HTML

Parametres:
  s -> El text que es vol austar
*/
function h($s=''){
	return stripslashes($s);
}

/*
Converteix el salt de linia en <br />

Parametres:
  s -> El text que es vol ajustar
*/
function lb($s=''){
	return preg_replace('/\r\n|\r/', "<br />", $s);
}

/*
Treu els accents i caracters extranys i substitueix els espais en blanc per '_'.

Parametres:
  s -> El text que es vol netejar
*/
function str_limpia($s){

	$patron=array(
	// Sustiutimos carácteres extraños
	'/"|&|<|>|¡|¢|£|¤/' => '_',
	'/¥|¦|§|¨|©|«|¬|­|®|¯/' => '_',
	'/±|&sup2;|&sup3;|´|µ|¶|·|÷/' => '_',
	'/°|&sup1;|»|&frac14;|&frac12;|&frac34;|¿/' => '_',
	// Sustituir vocales con signo por las vocales ordinarias.//
	'/à|á|â|ã|ä|å|æ|ª/' => 'a',
	'/À|Á|Â|Ã|Ä|Å|Æ/' => 'A',
	'/è|é|ê|ë|ð/' => 'e',
	'/È|É|Ê|Ë|Ð/' => 'E',
	'/ì|í|î|ï/' => 'i',
	'/Ì|Í|Î|Ï/' => 'I',
	'/ò|ó|ô|õ|ö|ø|º/' => 'o',
	'/Ò|Ó|Ô|Õ|Ö|Ø/' => 'o',
	'/ù|ú|û|ü/' => 'u',
	'/Ù|Ú|Û|Ü/' => 'U',
	// Algunas consonantes especiales como la ç, la y, la ñ y otras.//
	'/ç/' => 'c',
	'/Ç/' => 'C',
	'/ý|ÿ/' => 'y',
	'/Ý|Ÿ/' => 'Y',
	'/ñ/' => 'n',
	'/Ñ/' => 'N',
	'/þ/' => 't',
	'/Þ/' => 'T',
	'/ß/' => 's',
	'/ú/' => 'u',
	'/ /' => '-');
	
	$s=preg_replace(array_keys($patron), array_values($patron), $s);
	
	return $s;
}

function treu_tags($s){
	//$s = preg_replace('/&.+?;/', '', $s); // kill entities
	//$s = preg_replace('/[^%a-z0-9 _-]/', '', $s);
	
	return strip_tags($s);
}

function url_to_html_id(){
	$ret=_ROOT;
	$ret=str_replace("http://", "", $ret);
	$ret=str_replace("https://", "", $ret);
	$ret=str_replace("/", "-", $ret);
	$ret=str_replace(".", "-", $ret);
	$ret=str_replace("_", "-", $ret);
	$ret=url_neteja($ret);
	return $ret;
}

function sanitize_file_name( $name ) { // Like sanitize_title, but with periods
	$name = strtolower( $name );
	$name = preg_replace('/&.+?;/', '', $name); // kill entities
	$name = str_replace( '_', '-', $name );
	$name = preg_replace('/[^a-z0-9\s-.]/', '', $name);
	$name = preg_replace('/\s+/', '-', $name);
	$name = preg_replace('|-+|', '-', $name);
	$name = trim($name, '-');
	return $name;
}

/**
 * Sanitizes title or use fallback title.
 *
 * Specifically, HTML and PHP tags are stripped. Further actions can be added
 * via the plugin API. If $title is empty and $fallback_title is set, the latter
 * will be used.
 *
 * @since 1.0.0
 *
 * @param string $title The string to be sanitized.
 * @param string $fallback_title Optional. A title to use if $title is empty.
 * @return string The sanitized string.
 */
function url_neteja($s){
	$s = strip_tags($s);
	// Preserve escaped octets.
	$s = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $s);
	// Remove percent signs that are not part of an octet.
	$s = str_replace('%', '', $s);
	$s = str_replace('¡', '', $s);
	$s = str_replace('¿', '', $s);
	$s = str_replace('·', '', $s);
	// Restore octets.
	$s = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $s);

	$s = remove_accents($s);
	if (seems_utf8($s)) {
		if (function_exists('mb_strtolower')) {
			$s = mb_strtolower($s, 'UTF-8');
		}
		$s = utf8_uri_encode($s, 200);
	}

	$s = strtolower($s);
	$s = preg_replace('/&.+?;/', '', $s); // kill entities
	$s = preg_replace('/[^%a-z0-9 _-]/', '', $s);
	$s = preg_replace('/\s+/', '-', $s);
	$s = preg_replace('|-+|', '-', $s);
	$s = trim($s, '-');

	return $s;
}

/**
 * Checks to see if a string is utf8 encoded.
 *
 * @author bmorel at ssi dot fr
 *
 * @since 1.2.1
 *
 * @param string $Str The string to be checked
 * @return bool True if $Str fits a UTF-8 model, false otherwise.
 */
function seems_utf8($Str) { # by bmorel at ssi dot fr
	$length = strlen($Str);
	for ($i=0; $i < $length; $i++) {
		if (ord($Str[$i]) < 0x80) continue; # 0bbbbbbb
		elseif ((ord($Str[$i]) & 0xE0) == 0xC0) $n=1; # 110bbbbb
		elseif ((ord($Str[$i]) & 0xF0) == 0xE0) $n=2; # 1110bbbb
		elseif ((ord($Str[$i]) & 0xF8) == 0xF0) $n=3; # 11110bbb
		elseif ((ord($Str[$i]) & 0xFC) == 0xF8) $n=4; # 111110bb
		elseif ((ord($Str[$i]) & 0xFE) == 0xFC) $n=5; # 1111110b
		else return false; # Does not match any model
		for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
			if ((++$i == $length) || ((ord($Str[$i]) & 0xC0) != 0x80))
			return false;
		}
	}
	return true;
}

/**
 * Encode the Unicode values to be used in the URI.
 *
 * @since 1.5.0
 *
 * @param string $utf8_string
 * @param int $length Max length of the string
 * @return string String with Unicode encoded for URI.
 */
function utf8_uri_encode( $utf8_string, $length = 0 ) {
	$unicode = '';
	$values = array();
	$num_octets = 1;
	$unicode_length = 0;

	$string_length = strlen( $utf8_string );
	for ($i = 0; $i < $string_length; $i++ ) {

		$value = ord( $utf8_string[ $i ] );

		if ( $value < 128 ) {
			if ( $length && ( $unicode_length >= $length ) )
				break;
			$unicode .= chr($value);
			$unicode_length++;
		} else {
			if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

			$values[] = $value;

			if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
				break;
			if ( count( $values ) == $num_octets ) {
				if ($num_octets == 3) {
					$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
					$unicode_length += 9;
				} else {
					$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
					$unicode_length += 6;
				}

				$values = array();
				$num_octets = 1;
			}
		}
	}

	return $unicode;
}

/**
 * Converts all accent characters to ASCII characters.
 *
 * If there are no accent characters, then the string given is just returned.
 *
 * @since 1.2.1
 *
 * @param string $string Text that might have accent characters
 * @return string Filtered string with replaced "nice" characters.
 */
function remove_accents($string) {
	if ( !preg_match('/[\x80-\xff]/', $string) )
		return $string;

	if (seems_utf8($string)) {
		$chars = array(
		// Decompositions for Latin-1 Supplement
		chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
		chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
		chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
		chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
		chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
		chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
		chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
		chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
		chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
		chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
		chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
		chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
		chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
		chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
		chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
		chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
		chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
		chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
		chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
		chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
		chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
		chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
		chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
		chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
		chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
		chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
		chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
		chr(195).chr(191) => 'y',
		// Decompositions for Latin Extended-A
		chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
		chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
		chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
		chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
		chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
		chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
		chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
		chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
		chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
		chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
		chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
		chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
		chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
		chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
		chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
		chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
		chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
		chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
		chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
		chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
		chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
		chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
		chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
		chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
		chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
		chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
		chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
		chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
		chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
		chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
		chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
		chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
		chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
		chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
		chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
		chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
		chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
		chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
		chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
		chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
		chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
		chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
		chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
		chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
		chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
		chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
		chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
		chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
		chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
		chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
		chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
		chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
		chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
		chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
		chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
		chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
		chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
		chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
		chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
		chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
		chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
		chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
		chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
		chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
		// Euro Sign
		chr(226).chr(130).chr(172) => 'E',
		// GBP (Pound) Sign
		chr(194).chr(163) => '');

		$string = strtr($string, $chars);
	} else {
		// Assume ISO-8859-1 if not UTF-8
		$chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
			.chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
			.chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
			.chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
			.chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
			.chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
			.chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
			.chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
			.chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
			.chr(252).chr(253).chr(255);

		$chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

		$string = strtr($string, $chars['in'], $chars['out']);
		$double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
		$double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
		$string = str_replace($double_chars['in'], $double_chars['out'], $string);
	}

	return $string;
}

/*
Retorna les primeres n paraules d'un text i hi afegeix un text al final

Parametres:
  $txt: El text que es vol resumir
	$n: Nº de paraules a mostrar
	$msg: el text que s'afegirà al final
*/
function resume($text, $n, $msg){	
	$text = strip_tags($text, '<br>');
	$words = explode(' ', $text, $n + 1);
	
	if (count($words) > $n) {
		array_pop($words);
		array_push($words, $msg);
		$text = implode(' ', $words);
	}
	
	return $text;
}

/*
* Retalla un string a un mida donada. Ho fe servir per noms d'arxius, per exemple, que no tenen espais i si son llargs no salten de linia i osbresurten.
* $str: L'string que es vol retallar.
* $n: Número de caracters máxims.
*/
function retallaStr($str='', $n=20){
	$max=strlen($str);
	
	if($max>$n && $max>5){
		$mig=floor($n/2)-2;
		return substr($str, 0, $mig).'...'.substr($str, $max-$mig-2, $max);
	}else{
		return $str;
	}
}

/*
Afegeix '%' a un string per a fer cerques sql amb LIKE

Parametres:
  txt -> El string
*/
function pb($txt=''){
	return str_replace(' ', '%', $txt);
}

function isMail($m){
	if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $m)) {
		return true;
	}else{
		return false;
	}
}
/*
*	Añadimos maskara a la cadena dada
*	$str = cadena
*	$len = longitud
*	$msk = mascara
*/

function setMask($str='', $ret=false, $len=6, $msk=0) {
	
	if ($ret) { 
		return str_pad($str, $len, $msk, STR_PAD_LEFT); 
	}else{ 
		e(str_pad($str, $len, $msk, STR_PAD_LEFT)); 
	}
	 
}


/***************************************************************************************************************************************************************
* SQL
***************************************************************************************************************************************************************/

/**
* Prepara un estring per a ser utilitzat en una consulta SQL amb un LIKE.
*	Separa l'string en paraules i hi afegeix un '%' entre cada paraula i a les puntes.
*/
function sql_like($str){
	$ar=explode(' ', $str);
	return "%".implode('%', $ar)."%";
}


/***************************************************************************************************************************************************************
* ORDENACIÓ
***************************************************************************************************************************************************************/

/*
*	Per a ordenar les taules.
* Retorna el sentit contrari alq ue li passem
*	$o=Ordre actual
*/
function getOrdre($o='ASC'){
	if($o=='DESC') return 'ASC';
	else return 'DESC';
}

/*
*	Per a ordenar les taules.
* escriu un simbol (o imatge) indicat que el camp esta ordenat i el seu sentit.
* $cc=columna de la taula
* $c=camp ordenat actual
* $o=Sentit de l'ordenació
*/
function getOrderImg($cc, $c, $o){
	if($cc==$c){
		if($o=='ASC') echo "<img src=\"../img/adm/down_b.gif\" alt=\"\" />";
		else echo "<img src=\"../img/adm/up_b.gif\" alt=\"\" />";
	}
}


/***************************************************************************************************************************************************************
* ARXIUS I DIRECTORIS
***************************************************************************************************************************************************************/

/*
Crea un directori

Parametres:
  path -> El drectori a crear
	rights -> El permisos del nou directori
*/
function mk_dir($path){
	$folder_path = array($path);
	
  while(!@is_dir(dirname(end($folder_path)))
         && dirname(end($folder_path)) != '/'
         && dirname(end($folder_path)) != '.'
         && dirname(end($folder_path)) != '')
   array_push($folder_path, dirname(end($folder_path)));

  while($parent_folder_path = array_pop($folder_path))
		 if(!@mkdir($parent_folder_path, 0777))
			 user_error("No se puede crear el directorio \"$parent_folder_path\".");
}

/*
Elimina un directori i tot el que hi hagi a dins

Parametres:
  path -> El directori a borrar
*/
function rm_dir($path) {
   // Add trailing slash to $path if one is not there
   if(file_exists($path)) {	  
	   if (substr($path, -1, 1) != "/" && substr($path, -1, 1) != "\\") {
		   $path .= "/";
	   }
	
	   $normal_files = glob($path . "*");
	   $hidden_files = glob($path . "\.?*");
	   $all_files = array_merge($normal_files, $hidden_files);
	
	   foreach ($all_files as $file) {
		   # Skip pseudo links to current and parent dirs (./ and ../).
		   if (preg_match("/(\.|\.\.)$/", $file))
		   {
				   continue;
		   }
	
		   if (is_file($file) === TRUE) {
			   // Remove each file in this Directory
			   unlink($file);
		   }
		   else if (is_dir($file) === TRUE) {
			   // If this Directory contains a Subdirectory, run this Function on it
			   rm_dir($file);
		   }
	   }
	   // Remove Directory once Files have been removed (If Exists)
	   if (is_dir($path) === TRUE) {
		   rmdir($path);
	   }
   }
}

/*
* Puja arxius
*
* $img: $HTTP_POST_FILES['file']
* $path: Directori on es guardarà la l'arxiu
* $tipus: Tipus d'arxius permesos (Array)
*/
function upload($img, $path, $tipus){
	$ret=1;
	
	if($img['name']!=""){
		$nom_arxiu = $img['name'];
		$tip_arxiu = $img['type'];
		$tam_arxiu = $img['size'];
		$ok=false;

		foreach($tipus as $t){
			if($t==$tip_arxiu) $ok=true;
		}

		if($ok){
			rm_dir($path);
			mk_dir($path);

			if(!move_uploaded_file($img['tmp_name'], $path.str_neteja($nom_arxiu))){
				 $ret=0;
			}
		}else{
			$ret=2;
		}
	}
	
	return $ret;
}

// Movemos archivo desde temporal a carpeta indicada
// y hacemos miniaturas
function move_file($img, $path, $sc=0, $tnw=0, $tnh=0){
	if (!$sc)  $sc=_SC_SIZE;
	if (!$tnw) $tnw=_TH_SIZE_W;
	if (!$tnh) $tnh=_TH_SIZE_H;	
	$ret = false;
	rm_dir($path);
	if (!file_exists($path)) mk_dir($path);
	$tmp_path=ini_get("upload_tmp_dir")."/plupload/".$img;
	//chmod($tmp_path, 0777);
	//chmod($path, 0777);
	if(@rename($tmp_path, $path.$img)){
		square_crop($path.$img, $path."sc_".$img, $sc);
		resizeImage($path.$img, $tnh, $tnw, $path."th_".$img);
		$ret=true;
	}
	return $ret;	
}

// Movemos archivo desde temporal
function move_file_gal($img, $path, $sc=0, $tnw=0, $tnh=0){
	if (!$sc)  $sc=_SC_SIZE;
	if (!$tnw) $tnw=_TH_SIZE_W;
	if (!$tnh) $tnh=_TH_SIZE_H;
	$ret = false;
	if (!file_exists($path)) mk_dir($path);
	$tmp_path=ini_get("upload_tmp_dir")."/plupload/".$img;
	//chmod($tmp_path, 0777);
	//chmod($path, 0777);
	if(@rename($tmp_path, $path.$img)){
		square_crop($path.$img, $path."sc_".$img, $sc);
		resizeImage($path.$img, $tnh, $tnw, $path."th_".$img);
		$ret=true;
	}
	return $ret;	
}
 
// Recuperamos extension del archivo
function extension($archivo){
	$a=explode(".",$archivo);
	return $a[count($a)-1];
} 

/*
* Retorna el tamany d'un arxiu en B, kB, MB, etc
*
* $f: Ruta de l'arxiu
* return: Tamany de l'arxiu amb format.
*/
function tamano($f=""){
	$size=filesize($f);
	$sizes=array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$retstring='%01.2f %s';
	$lastsizestring=end($sizes);
	
	foreach($sizes as $sizestring){
		if($size<1024){ break; }
		if($sizestring!=$lastsizestring){ $size/=1024; }
	}
	
	if($sizestring==$sizes[0]){ $retstring='%01d %s'; } // Bytes aren't normally fractional
	return sprintf($retstring, $size, $sizestring);
}

function descarga($file){
	if ((isset($file))&&(file_exists($file))) {
		header("Content-length: ".filesize($file));
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $file . '"');
		readfile("$file");
	}else{
		echo "No se ha seleccionado ningún fichero";
	}
}


/***************************************************************************************************************************************************************
* URI
***************************************************************************************************************************************************************/

/*
Redirecciona a la pagina especificada, amb javascript

Parametres:
  url -> Pagina on es vol redireccionar
*/
function redirijie($url){
	return "<script type='text/javascript' language='javascript'>window.location.href='$url'</script>";
}

/*
Redirecciona a la pagina especificada, amb header

Parametres:
  url -> Pagina on es vol redireccionar
*/
function redirijieH($url){
	header("Location: $url");
	exit(0);
}

function addUriParam($p){
	if(count($_GET)==0){
		return $_SERVER['SCRIPT_NAME']."?".$p;
	}else{
		return $_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING']."&amp;".$p;
	}
}

function selfURL(){
	$s=empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
	$protocol=strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
	
	if(!isset($_SERVER['REQUEST_URI'])){
		$_SERVER['REQUEST_URI'] = "/".substr($_SERVER['PHP_SELF'],1 );
    if(isset($_SERVER['QUERY_STRING'])){ $_SERVER['REQUEST_URI'].='?'.$_SERVER['QUERY_STRING'];}
	}
	
	return $protocol."://".$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"]; 
} 

function strleft($s1, $s2) { 
	return substr($s1, 0, strpos($s1, $s2)); 
}


/***************************************************************************************************************************************************************
* DATA I HORA
***************************************************************************************************************************************************************/

/*
* Formateja una data
* 
* $d: La data
* $f: Format de la data que es vol
* return: La data amb el format especificat
*/
function getData($d, $f='d/m/Y'){
	return date($f,strtotime($d));
}

function getDataF($d, $f='%d/%m/%Y'){
	return strftime($f,strtotime($d));
}

/*
* Formateja una hora
* 
* $d: La hora
* $f: Format de la hora que es vol
* return: La hora amb el format especificat
*/
function getHora($h, $f='H:i'){
	return date($f,strtotime($h));
}

function strToData($fecha){
    //preg_match( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
    $mifecha=explode("-", $fecha);
	$lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0];
    return $lafecha;
}

function strToDataDB($fecha){
   // preg_match( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha);
	$mifecha=explode("/", $fecha);
    $lafecha=$mifecha[2]."-".$mifecha[1]."-".$mifecha[0];
	return $lafecha;
}


/***************************************************************************************************************************************************************
* IMATGES
***************************************************************************************************************************************************************/

/*
* Redimensiona una imatge i en fa una copia
*
* $filename: Imatge original
* $max_width: Amplada maxima
* $max_height: Alçada maxima
* $newfilename: Imatge resultant
* $withSampling: ----
*/
function resizeImage($filename,$max_width,$max_height='',$newfilename="",$withSampling=true){
    if($newfilename=="") $newfilename=$filename;
    // Get new sizes
    list($width, $height)=getimagesize($filename);
    //-- dont resize if the width of the image is smaller or equal than the new size.
    if($width<=$max_width) $max_width=$width;
    
		if($width>=$height) $percent = $max_width/$width;
		else $percent = $max_height/$height;
   
    $newwidth = $width * $percent;
		$newheight = $height * $percent;
    
    // Load
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    $ext = strtolower(getExtension($filename));
   
    if($ext=='jpg' || $ext=='jpeg') $source = imagecreatefromjpeg($filename);
    if($ext=='gif') $source = imagecreatefromgif($filename);
    if($ext=='png') $source = imagecreatefrompng($filename);
   
    // Resize
    if($withSampling) imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    else imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
       
    // Output
    if($ext=='jpg' || $ext=='jpeg') return imagejpeg($thumb,$newfilename);
    if($ext=='gif') return imagegif($thumb,$newfilename);
    if($ext=='png') return imagepng($thumb,$newfilename);
}

function resampimagejpg( $forcedwidth, $forcedheight, $sourcefile, $destfile )
{
    $fw = $forcedwidth;
    $fh = $forcedheight;
    $is = getimagesize( $sourcefile );
    if( $is[0] >= $is[1] )
    {
        $orientation = 0;
    }
    else
    {
        $orientation = 1;
        $fw = $forcedheight;
        $fh = $forcedwidth;
    }
    if ( $is[0] > $fw || $is[1] > $fh )
    {
        if( ( $is[0] - $fw ) >= ( $is[1] - $fh ) )
        {
            $iw = $fw;
            $ih = ( $fw / $is[0] ) * $is[1];
        }
        else
        {
            $ih = $fh;
            $iw = ( $ih / $is[1] ) * $is[0];
        }
        $t = 1;
    }
    else
    {
        $iw = $is[0];
        $ih = $is[1];
        $t = 2;
    }
    if ( $t == 1 )
    {
        $img_src = imagecreatefromjpeg( $sourcefile );
        $img_dst = imagecreatetruecolor( $iw, $ih );
        imagecopyresampled( $img_dst, $img_src, 0, 0, 0, 0, $iw, $ih, $is[0], $is[1] );
        if( !imagejpeg( $img_dst, $destfile, 90 ) )
        {
            exit( );
        }
    }
    else if ( $t == 2 )
    {
        copy( $sourcefile, $destfile );
    }
}

/*
Funció per escalar imatges proporcionalment.

Parametres:
	$width : Amplada real de la iamtge
	$height : Alçada real de la iamtge
	$targeth : Alçada màxima de la imatge escalada
	$targetw : Amplada màxima de la imatge escalada


Exemple:

<?php
	$mysock = getimagesize("imgs/imatge.jpg"); 
?>

<img src="imgs/imatge.jpg" <?php echo imageResize($mysock[0], $mysock[1], 100, 150); ?> border="0">
*/
function imageResize($width, $height, $targeth, $targetw) {
	if($width<=$targetw && $height<=$targeth){
		return "width=\"$width\" height=\"$height\"";
	}else{
		//takes the larger size of the width and height and applies the formula accordingly...this is so this script will work dynamically with any size image
		if ($width > $height) {
			$percentage = ($targetw / $width);
		} else {
			$percentage = ($targeth / $height);
		}
		
		//gets the new value and applies the percentage, then rounds the value
		$width = round($width * $percentage);
		$height = round($height * $percentage);
		
		
		/** AFEGIT 18/10/2007 ***********************************************/
		if($width>$targetw){
			$percentage = ($targetw / $width);
			$width = round($width * $percentage);
			$height = round($height * $percentage);
		}elseif($height>$targeth){
			$percentage = ($targeth / $height);
			$width = round($width * $percentage);
			$height = round($height * $percentage);
		}
		/********************************************************************/
		
		//returns the new sizes in html image tag format...this is so you can plug this function inside an image tag and just get the
		return "width=\"$width\" height=\"$height\"";
	}
}

/*	Realiza crop cuadrado de la imagen	*/

function square_crop($src_image, $dest_image, $thumb_size = 64, $jpg_quality = 90) {

    // Get dimensions of existing image
    $image = getimagesize($src_image);

    // Check for valid dimensions
    if( $image[0] <= 0 || $image[1] <= 0 ) return false;

    // Determine format from MIME-Type
    $image['format'] = strtolower(preg_replace('/^.*?\//', '', $image['mime']));

    // Import image
    switch( $image['format'] ) {
        case 'jpg':
        case 'jpeg':
            $image_data = imagecreatefromjpeg($src_image);
        break;
        case 'png':
            $image_data = imagecreatefrompng($src_image);
        break;
        case 'gif':
            $image_data = imagecreatefromgif($src_image);
        break;
        default:
            // Unsupported format
            return false;
        break;
    }

    // Verify import
    if( $image_data == false ) return false;

    // Calculate measurements
    if( $image[0] && $image[1] ) {
        // For landscape images
        $x_offset = ($image[0] - $image[1]) / 2;
        $y_offset = 0;
        $square_size = $image[0] - ($x_offset * 2);
    } else {
        // For portrait and square images
        $x_offset = 0;
        $y_offset = ($image[1] - $image[0]) / 2;
        $square_size = $image[1] - ($y_offset * 2);
    }

    // Resize and crop
    $canvas = imagecreatetruecolor($thumb_size, $thumb_size);
    if( imagecopyresampled(
        $canvas,
        $image_data,
        0,
        0,
        $x_offset,
        $y_offset,
        $thumb_size,
        $thumb_size,
        $square_size,
        $square_size
    )) {

        // Create thumbnail
        switch( strtolower(preg_replace('/^.*\./', '', $dest_image)) ) {
            case 'jpg':
            case 'jpeg':
                return imagejpeg($canvas, $dest_image, $jpg_quality);
            break;
            case 'png':
                return imagepng($canvas, $dest_image);
            break;
            case 'gif':
                return imagegif($canvas, $dest_image);
            break;
            default:
                // Unsupported format
                return false;
            break;
        }

    } else {
        return false;
    }

}


/***************************************************************************************************************************************************************
* VARIS
***************************************************************************************************************************************************************/

/*
* Genera una contrasenya aleatoriament
*/
function generatePassword($length=9, $strength=0) {
	$vowels = 'aeuy';
	$consonants = 'bdghjmnpqrstvz';
	if($strength & 1) $consonants.='BDGHJLMNPQRSTVWXZ';
	if($strength & 2)	$vowels .= "AEUY";
	if($strength & 4) $consonants .= '23456789';
	if($strength & 8) $consonants .= '@#$%';
	$password = '';
	$alt = time() % 2;
	
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
		} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
		}
	}
	
	return $password;
}

/*
*	Encripta un string usando el _SEED mediante CBC mode
*	Devuleve string codificada en base64
*	http://es2.php.net/manual/en/function.mcrypt-cbc.php
*/

function encrypt($string, $key=_SEED) {
    $enc = "";
    global $iv;
    //$enc=mcrypt_cbc (MCRYPT_TripleDES, $key, $string, MCRYPT_ENCRYPT, $iv);
	mcrypt_generic_init(_CIPHER, _SEED, _IV);
	$enc=mcrypt_generic(_CIPHER,$string);
	mcrypt_generic_deinit(_CIPHER);
  return base64_encode($enc);
}

/*
*	Desencripta un string usando el _SEED mediante CBC mode
*	Devuleve string 
*	http://es2.php.net/manual/en/function.mcrypt-cbc.php
*/

function decrypt($string, $key=_SEED) {
    $dec = "";
    $string = (base64_decode(($string)));
    //global $iv;
    //$dec = mcrypt_cbc (MCRYPT_TripleDES, $key, $string, MCRYPT_DECRYPT, $iv);
	mcrypt_generic_init(_CIPHER, _SEED, _IV);
	$dec=mdecrypt_generic(_CIPHER,$string);
	mcrypt_generic_deinit(_CIPHER);
  return $dec;
}


/**
* Retorna el nom i la inicial del cognom.
*/
function getNomCurt($nom, $cog){
	$n=ucfirst($nom);
	
	if($cog!='' && !is_null($cog)){
		$n.=" ".strtoupper(substr($cog, 0, 1)).".";
	}
		
	return $n;
}

/*
*	Funciones para encripta y desencriptar
*	IMPORTANTE: es necesario tener la libreria mcrypt instalada en PHP
*	Codifica y decodifica respuesta y entradas a base64, para facilitar envío por url
*/

function cript($cadena, $clave = _SEED)
{
	$cifrado = MCRYPT_RIJNDAEL_256;
	$modo = MCRYPT_MODE_ECB;
	$ret =  mcrypt_encrypt($cifrado, $clave, $cadena, $modo,
		mcrypt_create_iv(mcrypt_get_iv_size($cifrado, $modo), MCRYPT_RAND)
		);
	return base64_encode($ret);
}

function decript($cadena, $clave = _SEED)
{
	$cadena=base64_decode($cadena);
	$cifrado = MCRYPT_RIJNDAEL_256;
	$modo = MCRYPT_MODE_ECB;
	$ret =  mcrypt_decrypt($cifrado, $clave, $cadena, $modo,
		mcrypt_create_iv(mcrypt_get_iv_size($cifrado, $modo), MCRYPT_RAND)
		);
	return $ret;
}

// Recuperamos array de idiomas para editarlos

function getLanguages() {
	global $_LANGS;
	$i=0;
	e("<div id=\"edit_lang\">");
	foreach ($_LANGS as $k => $v) {
		if ($i>0) { 
			e("&nbsp;|&nbsp;");
			e("<a href=\"#\" id=\"transitem_".$k."\" class=\"edit_lang\" title=\"Editar ".$v[1]."\" lang=\"".$k."\">".strtoupper($v[0])."</a>");
		}
		$i++;
	}
	e("</div>");
}

// Recuperamos miniaturas de una imagen desde tinyMce
// Descomprimimos array y añadimos nuevas variables.
function getTinyTh($img='', $tn='mid') {
	if ($img!="") {
		$ori=$img;
		$path=explode("/", $img);
		// Si no es miniatura realizamos cambios
		if (!in_array("mid", $path) && !in_array("tn", $path)) {
			$img=$tn."/".$tn."_".$path[(sizeOf($path)-1)];
			array_pop($path);
			
			$url="";
			foreach($path as $item) { $url.=$item."/"; }
				if(file_exists($_SERVER['DOCUMENT_ROOT'].$url.$img)) { e($url.$img); }else{ e($ori); }
		}else{
			e($img);
		}
	}
}


function setReplace($str="", $srch="", $rep="") {
	if (!is_array($srch)) $srch=array("/&ldquo;/", "/&rdquo;/", "/&quot;/", "/&Ntilde;/", "/&ntilde;/", "/&Aacute;/", "/&aacute;/", "/&Eacute;/", "/&eacute;/", "/&Iacute;/", "/&iacute;/", "/&Oacute;/", "/&oacute;/", "/&Uacute;/", "/&uacute;/", "/&Agrave;/", "/&agrave;/", "/&Egrave;/", "/&egrave;/", "/&Igrave;/", "/&igrave;/", "/&Ograve;/", "/&ograve;/", "/&Ugrave;/", "/&ugrave;/");
	if (!is_array($rep)) $rep=array('', '', '', 'Ñ', 'ñ', 'Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ú', 'ú', 'À', 'à', 'È', 'è', 'Ì', 'ì', 'Ò', 'ò', 'Ù', 'ù');
	
	$str=preg_replace($srch, $rep, $str);
	return $str;
}

function setReplaceJSON($str="", $srch="", $rep="") {
	if (!is_array($srch)) $srch=array("/&ldquo;/", "/&rdquo;/", "/&quot;/", "/&Ntilde;/", "/&ntilde;/", "/&Aacute;/", "/&aacute;/", "/&Eacute;/", "/&eacute;/", "/&Iacute;/", "/&iacute;/", "/&Oacute;/", "/&oacute;/", "/&Uacute;/", "/&uacute;/", "/&Agrave;/", "/&agrave;/", "/&Egrave;/", "/&egrave;/", "/&Igrave;/", "/&igrave;/", "/&Ograve;/", "/&ograve;/", "/&Ugrave;/", "/&ugrave;/");
	if (!is_array($rep)) $rep=array('', '', '', 'Ñ', 'ñ', 'Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ú', 'ú', 'À', 'à', 'È', 'è', 'Ì', 'ì', 'Ò', 'ò', 'Ù', 'ù');
	
	$str=preg_replace($srch, $rep, $str);
	return $str;
}

/*************************************************************************************************************
*  Función para Description
*  Este consiste en un resumen del contenido de la página y el cual no debe excederse de 250 caracteres,
*  entonces crearemos una función que elimine las etiquetas de HTML y que luego cortamos el texto a 247
*  caracteres y concatenamos tres puntos al final.
*************************************************************************************************************/

function getMetaDescription($text) {
	$text = setReplace($text);
	//$text = utf8_decode($text);
	$text = strip_tags($text);
	$text = trim($text);
	$text = substr($text, 0, 247);
	return $text."...";
}

/*************************************************************************************************************
*  Función para Keywords
*  Para obtener las palabras clave o keywords primero limpiamos el texto, luego extraemos todas las palabras
*  a un array para luego contar uno a uno la cantidad de veces que se repite, omitiendo las palabras de menos
*  de tres caracteres. Luego ordenamos el array por la cantidad de veces que repite la palabra para finalmente
*  mostrar los 10 primeras separadas por comas.
*************************************************************************************************************/

function getMetaKeywords($text) {
	// Limpiamos el texto
	$text = setReplace($text);
	$text = strip_tags($text);
	$text = strtolower($text);
	$text = trim($text);
	$text = preg_replace('/[^a-zA-Z0-9 -]/', ' ', $text);
	// extraemos las palabras
	$match = explode(" ", $text);
	// contamos las palabras
	$count = array();
	if (is_array($match)) {
		foreach ($match as $key => $val) {
			if (strlen($val)>= 5) {
				if (isset($count[$val])) {
					$count[$val]++;
				} else {
					$count[$val] = 1;
				}
			}
		}
	}
	// Ordenamos los totales
	arsort($count);
	$count = array_slice($count, 0, 10);
	return implode(", ", array_keys($count));
}

?>