<?php
require("xml2array.php");
require("functions.php");

$que = getVariable($_GET['city'], "lima");
$url = "http://www.google.com/ig/api?weather=".urlencode($que)."&hl=es";

$contents = file_get_contents($url);
$data = xml2array($contents);

$weather_info = $data['xml_api_reply']['weather']['forecast_information'];
$weather_current = $data['xml_api_reply']['weather']['current_conditions'];
$weather_forecast = $data['xml_api_reply']['weather']['forecast_conditions'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
</head>
<body>
<?php if (sizeof($weather_forecast) == 0) { ?>
Estamos mejorando el sistema para ofrecerle un servicio de calidad para <?=$que?> 
<?php } else { ?>
<?php 
$today = getdate(); 
$hora=$today["hours"]; 
if ($hora<6) { 
echo(" Buenos dias  "); 
}elseif($hora<12){ 
echo(" Buenos dias "); 
}elseif($hora<=18){ 
echo("Buenas tardes "); 
}else{ echo("Buenas noches "); } 
?> El tiempo hoy en 
  su ciudad  es <?php echo $weather_current['condition']['attr']['data']; ?> temperatura: <?php echo $weather_current['temp_c']['attr']['data']; ?> &deg;C <?php echo $weather_current['humidity']['attr']['data']; ?>

<?php } ?>
</body>
</html>