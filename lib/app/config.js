var config = {

    /* configuracion general */
    app:
    {

        titulo: "SEAT EXPERIENCE",
        lengua: false,
        ulrws: "http://ws.seatexperience.net/",
        urlSoap: "http://ws.seatexperience.net/client.php?",
        versionWS: "ws1",
        color: null,
        iduser:null,
        dinamic_Data:null,
        contador: 5,
        evento_default: "30",
        login:false,
        idiomaContent:null,
        grap:true,
        minutosInactividadTelemetria:5
    },
    permisos:
    {
 
        loginInvitado:"",
        selector:"",
        dinamicas:"",
        videos:"",
        tecnicas:"none",
        reservas:"none",
        waypoints:"none",
        roadbooks:"none",
        prensa:"",
        agenda:"",
        clima:"",
        telemetria:"",
        telefonos:"",
        interes:"",
        pie:""
    },
    conexion:
    {

        tipo: null
    },
    intervalos:
    {
        intervaloGeolocalizacion: 300,
        intervalotracking: 1500,
        intervaloVelocidad: 2000,
        intervaloGetcity: 10000,
        loaderEmpty: 1000,
        storage:{
          intervalo_a:null,
          intervalo_b:null,
          intervalo_c:null 
        }

    },
    coord:
    {
        lat:null,
        lon:null,
        lugar:"Barcelona",

    },
    tracking:
    {
        velocidad:0,
        maxvelocidad:0,
        session:null,
        usuario:null,
        latitud:0,
        longitud:0,
        GPSignal:0,
        rumbo:0,
        fuerza:0,
        altitud:0,
        aceleracionX:0,
        aceleracionY:0,
        aceleracionZ:0,
        timeStart:0

    },
    idioma: 
    {

        es:{
            lang1:"Hola invitado!",

        },
        en:{

            lang1:"HI invitado!",

        }
    },
    google: {

        analytics: 'UA-63354014-1',
        urlmap: "http://maps.googleapis.com/maps/api/geocode"
    },
}

var data = {
    
    app:
    {
         get:'43',
         name:'app'
    }
}