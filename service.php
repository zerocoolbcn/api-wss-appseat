<?php
  
  header('Access-Control-Allow-Origin: *');  


	ini_set("soap.wsdl_cache_enabled", "0");
	include(dirname(dirname(__FILE__))."/public_html/library/mysql/mysql.php");
	include(dirname(dirname(__FILE__))."/public_html/lib/nusoap.php");


	$servidor = new soap_server;

	$ns = "http://ws.lapexperience.com";
	$servidor->configureWSDL('_LapExperience_WS',$ns);

  function getLenguaje($lang)
  {

    $HTMLexit = file_get_contents('http://ws.lapexperience.com/lang/'.$lang.'.json');
    $arreglo[] = array('lang'=>$lang);
    return  $arreglo;
    
  }

  function getContent($params,$iduser,$idevent,$lang,$id,$login)
  {
     $getdata = http_build_query(
      array(
       'iduser'=>$iduser,
       'idevento'=>$idevent,
       'contenido'=>$params,
       'id'=>$id,
       'lang'=>$lang,
       'login'=>$login
       )
      );
      
      $opts = array('http' =>
       array(
        'method'  => 'GET',
        'content' => $getdata
      )
      );
      
    $context  = stream_context_create($opts);
    $HTMLexit = file_get_contents('http://ws.lapexperience.com/views/?'.$getdata, false, $context);
    $arreglo[] = array('html'=>base64_encode($HTMLexit));
    return  $arreglo;
  }

  function sendLogin($username,$password)
  {



  }
  //configurar la estructura de los datos, 
//este arreglo es de tipo asociativo y contiene el nombre y tipo de dato.
$servidor->wsdl->addComplexType(
        'EstructuraHTML',
        'complexType',
        'struct',
        'all',
        '',
          array(
            'html' => array('name' => 'html', 'type' => 'xsd:string')
      
            )
      );

 $servidor->wsdl->addComplexType(
        'EstructuraLangArray',
        'complexType',
        'struct',
        'all',
        '',
          array(
            'lang' => array('name' => 'lang', 'type' => 'xsd:string')
      
            )
      );

//configurar arreglo de la estructura
$servidor->wsdl->addComplexType(
      'EstructuraHTMLarray',
      'complexType',
      'array',
      'sequence',
      'http://schemas.xmlsoap.org/soap/encoding/:Array',
      array(),
      array(
        array('ref' => 'http://schemas.xmlsoap.org/soap/encoding/:arrayType',
          'wsdl:arrayType' => 'tns:EstructuraHTML[]'
        )
      ),'tns:EstructuraHTML');
 


 $servidor->wsdl->addComplexType(
      'EstructuraLangArray',
      'complexType',
      'array',
      'sequence',
      'http://schemas.xmlsoap.org/soap/encoding/:Array',
      array(),
      array(
        array('ref' => 'http://schemas.xmlsoap.org/soap/encoding/:arrayType',
          'wsdl:arrayType' => 'tns:EstructuraLangArray[]'
        )
      ),'tns:EstructuraLangArray');


$servidor->register('getContent',
      array('content'=>'xsd:string','iduser'=>'xsd:string','idevent'=>'xsd:string','lang'=>'xsd:string'), //tipo de dato entrada
      array('return'=>'tns:EstructuraHTMLarray'), //tipo de dato salida
      $ns, false,
      'rpc', //tipo documento
      'literal', //tipo codificacion
      'Documentacion de getContent') ; //documentacion

$servidor->register('sendLogin',
      array('lang'=>'xsd:string'), //tipo de dato entrada
      array('return'=>'tns:EstructuraLangArray'), //tipo de dato salida
      $ns, false,
      'rpc', //tipo documento
      'literal', //tipo codificacion
      'Documentacion de getLenguajeLAP') ; //documentacion

$servidor->register('getLenguaje',
      array('lang'=>'xsd:string'), //tipo de dato entrada
      array('return'=>'tns:EstructuraLangArray'), //tipo de dato salida
      $ns, false,
      'rpc', //tipo documento
      'literal', //tipo codificacion
      'Documentacion de getLenguajeLAP') ; //documentacion

//Establecer servicio        
if (isset($HTTP_RAW_POST_DATA)) { 
  $input = $HTTP_RAW_POST_DATA; 
}else{ 
  $input = implode("\r\n", file('php://input')); 
}

$servidor->service($input);
?>