<?php

	session_start();
	
	header('Access-Control-Allow-Origin: *');
	
	$mode_bug = false;
	
	if($mode_bug==true){
		error_reporting(E_ALL);
		ini_set("display_errors", 1);	
	}else
	{
		error_reporting(0);	
	}
	/////////////////////////////////////////
	// SDK BASE 
	//  ----------------------------------
	/////////////////////////////////////////	
	include(dirname(dirname(__FILE__))."/public_html/library/Api/Bootstrap.php");
	include(dirname(dirname(__FILE__))."/public_html/library/MyApi/MyApi.php");
	include(dirname(dirname(__FILE__))."/public_html/library/Json/Encoder.php");
	include(dirname(dirname(__FILE__))."/public_html/library/Xml/xml.php");
	include(dirname(dirname(__FILE__))."/public_html/library/mysql/mysql.php");
	include(dirname(dirname(__FILE__))."/public_html/library/Log/Debug.class.php");
	///////////////////////////////////////
	
	
	$transcode = uniqid();

	$Debug = DebugLog::getInstance(); 
	

	Database::conecta();
	
	
	$request = new Api_Request($domain);
	
	$request->setParams(array("key", "version", "lenguaje",  "service", "format", "param"));
	

	$request->analyze();
	$api = new MyApi();
	
	$params =  $request->getParams();
	//
	// IDIOMAS
	include('lang/'.$params['lenguaje'].'_ws.php');
	////////////////////////////////////////
	////////////////////////////////////////
	$dia = @date("d");
	$mes = @date("m");
	$ano = @date("Y");	
	
	$Debug->SetLogPath(dirname(dirname(__FILE__))."/public_html/log/accesos/");
	$Debug->Debug("0001 CONEXION ESTABLECIDA :: ".$params['key']." :: version-> ".$params['version']." :: service-> ".$params['service']." :: format-> ".$params['format']." param-> ".$params['param']." :: codeID->".$transcode);
	
	if(!is_numeric($params['key']))
	{
		$login = TRUE;
		$datosUsuario = Database::query("SELECT * FROM usuarios WHERE keyapi='".$params['key']."'");
	
	}else
	{
		$datosUsuario[0]['id'] = $params['key'];
		$datosUsuario[0]['eventoDefault'] = $params['key'];
		$login = FALSE;
	}
	////////////////////////////////////////
	////////////////////////////////////////
	//
	$rute = "Services/".$params['version']."/".$params['service'].".php";

	///////////////////////////////////////
	require($rute);
	//
	if($_GET['api_response_type']=="xml"){
	
	header('Content-type: application/xml');
	
		echo arrayToXml($arr_respuesta);
		
	}elseif($_GET['api_response_type']=="json"){
	
	header('Content-type: application/json');
	
		echo json_encode($arr_respuesta);
		
	}elseif($_GET['api_response_type']=="serie"){
	
		echo serialize($arr_respuesta);
	}
	elseif($_GET['api_response_type']=="txt"){
	
		echo implode(',',$arr_respuesta);
	}
	Database::desconecta();
	exit(0);
?>
